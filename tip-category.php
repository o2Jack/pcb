<?php include_once('includes/header.php');

$imageArray = array("images/assembly-board.jpg","images/capabilities-board.jpg","images/other-board.jpg","images/thumb-1.jpg","images/thumb-2.jpg","images/thumb-3.jpg");

?>
<article class="page-content">
	<div class="grid-container">
		<div class="large-12">
			<div class="button-group float-right">
				<i id="icon"></i> <a id="input" href="">Choose an tip category </a>
				<ul id="dropdown-menu">
					<li><a href="tip-category.php">Category One</a></li>
					<li><a href="tip-category.php">Category Two</a></li>
					<li><a href="tip-category.php">Category Three </a></li>
					<li><a href="tip-category.php">Category Four</a></li>
					<li><a href="tip-category.php">Category Five</a></li>
				</ul>
			</div>
		</div>
		<div class="grid-x grid-padding-x">

			<div class="large-12 cell">
				<h2 class="bottom-line">Categories or index of tips...</h2>
			</div>

		</div>
	</div>
	<div class="grid-container">
		<div class="grid-x grid-padding-x small-1 medium-up-3">
			<div class="cell">
				<div class="card">
					<div class="card-divider">
						<h6>Standard Dielectric Stacks</h6>
					</div>
					<img src="<?php echo $imageArray[rand(0,5)]?>">
					<div class="card-section">
						<p>These are posted as guidelines only and other stacks may be used based on materials that are currently available</p>
						<a href="tip.php" class="button float-center">Read More</a>
					</div>
				</div>
			</div>
			<div class="cell">
				<div class="card">
					<div class="card-divider">
						<h6>Standard Dielectric Stacks</h6>
					</div>
					<img src="<?php echo $imageArray[rand(0,5)]?>">
					<div class="card-section">
						<p>These are posted as guidelines only and other stacks may be used based on materials that are currently available</p>
						<a href="tip.php" class="button float-center">Read More</a>
					</div>
				</div>
			</div>
			<div class="cell">
				<div class="card">
					<div class="card-divider">
						<h6>Standard Dielectric Stacks</h6>
					</div>
					<img src="<?php echo $imageArray[rand(0,5)]?>">
					<div class="card-section">
						<p>These are posted as guidelines only and other stacks may be used based on materials that are currently available</p>
						<a href="tip.php" class="button float-center">Read More</a>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-x grid-padding-x small-1 medium-up-3">
			<div class="cell">
				<div class="card">
					<div class="card-divider">
						<h6>Standard Dielectric Stacks</h6>
					</div>
					<img src="<?php echo $imageArray[rand(0,5)]?>">
					<div class="card-section">
						<p>These are posted as guidelines only and other stacks may be used based on materials that are currently available</p>
						<a href="tip.php" class="button float-center">Read More</a>
					</div>
				</div>
			</div>
			<div class="cell">
				<div class="card">
					<div class="card-divider">
						<h6>Standard Dielectric Stacks</h6>
					</div>
					<img src="<?php echo $imageArray[rand(0,5)]?>">
					<div class="card-section">
						<p>These are posted as guidelines only and other stacks may be used based on materials that are currently available</p>
						<a href="tip.php" class="button float-center">Read More</a>
					</div>
				</div>
			</div>
			<div class="cell">
				<div class="card">
					<div class="card-divider">
						<h6>Standard Dielectric Stacks</h6>
					</div>
					<img src="<?php echo $imageArray[rand(0,5)]?>">
					<div class="card-section">
						<p>These are posted as guidelines only and other stacks may be used based on materials that are currently available</p>
						<a href="tip.php" class="button float-center">Read More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php include_once('includes/footer.php'); ?>
