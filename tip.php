<?php include_once('includes/header.php'); ?>
<article class="page-content">
	<div class="grid-container">
		<div class="large-12 cell">
			<div class="button-group">
				<i id="icon"></i> <a id="input" href="">Choose an tip category </a>
				<ul id="dropdown-menu">
					<li><a href="tip-category.php">Category One</a></li>
					<li><a href="tip-category.php">Category Two</a></li>
					<li><a href="tip-category.php">Category Three </a></li>
					<li><a href="tip-category.php">Category Four</a></li>
					<li><a href="tip-category.php">Category Five</a></li>
				</ul>
			</div>
		</div>
		<div class="grid-x grid-padding-x">
			<div class="large-12 cell">
				<h2 class="bottom-line">Array Design Tips</h2>
			</div>

		</div>
		<div class="large-12">
			<p>The primary reason for having your boards delivered in an array is to make automated assembly faster and less expensive. Running an array of boards through a pick-and-place machine is far more efficient than sending them through one at a time. Arrays are also desirable because they allow the addition of tooling rails, tooling holes, and fiducials, all of which help your assembler.<img src="http://via.placeholder.com/275x231" align="right" class="pad-left" alt="Tip image">
			</p>
			<p>
				<strong>Tooling Rails</strong> are the 'frame' for the array. They provide stability and make it easier to handle the arrays throughout the assembly process. Tooling holes and fiducials are usually added to the tooling rails.
			</p>
			<p>
				<strong>Tooling Holes</strong> are non-plated holes added to the rails so the array can be pinned down to prevent unwanted shifting during assembly. They are typically 0.125" in diameter but can be drilled to your required specification.
			</p>
			<p>
				<strong>Fiducials</strong> are copper spots on the rails, which aid automated pick-and-place assembly equipment by providing a uniform reference point. The copper fiducial is typically 0.04" in diameter and will not be covered with mask to make it easier for assembly equipment to see.
			</p>
			<p>At PCB Universe, Inc., many of our customers are contract electronics manufacturers (CEMs). We have a lot of experience in setting up arrays for automated assembly. We can follow your array specifications or we can set up one for you. We will score where possible and tab route (route and retain) where it isn't. Many assemblers have guidelines as to how they prefer arrays to be constructed.&nbsp;Check with your assembler if they have size limitations or preferences regarding rail placement, tooling holes, or fiducials. Here are some guidelines and standards that will work for most contract assemblers.</p>
			<h4>Which array is best for you?</h4>
			<p>There are three types of arrays: Scored, Tab Routed, and a mixture of both. So how do you decide which one is right for your design?</p>
			<p>Scoring is the preferred choice for two reasons. Scoring has the advantage of a more consistently smooth board edge, and it wastes less material, which can mean cost savings especially for larger quantities or high layer count printed circuit boards where every square inch counts.</p>
			<p>Consider tab routing when your design has an irregular shape or if you need space between your boards to allow for overhanging components.</p>
			<p>A mix of scoring and tab routing can be used when some board sides are straight, which can be scored, while irregular sides must be tab routed.</p>
			<h5>In the absence of other instructions, our standard array handling is as follows:</h5>
			<p><img src="http://via.placeholder.com/350x150" class="float-right pad-right"></p>
			<strong>Scored Arrays</strong>
			<p>We will place 0.25" tooling rails along the two longest sides and there will be no space between the boards. The score will be made along the shared board outline. Scoring is performed only parallel to the x- and y-axes, not diagonally. Because scoring runs all the way across the array in a straight line, the board outline should be straight for scored arrays.
				<strong><a href="articles/Scoring Irregular Shaped Designs.png" target="_blank">Click here</a></strong> to see tips on how to setup scoring for irregular shapes.
			</p>
			<p>The score depth is approximately ⅓ of the total material thickness. Score lines will be made on the top and bottom sides of the PCB. This will leave a remaining web of material equal to approximately ⅓ of the total thickness of the board. If your board is 0.062" thick, the remaining material will be ~0.021".</p>
			<p>Standard PCB material is essentially fiberglass. Even though your board is scored, it will still be sturdy so be careful when separating the boards. Expect some amount of flexing before the score line will give. Some fiberglass strands are common along a scored edge. A quick bump with a belt sander will take care of any rough areas.</p>
			<strong>Jump Scoring</strong>
			<p>It is possible to stop a score from continuing all the way across an array; this is referred to as jump scoring. However, this is not a recommended array solution due to the limitations and expense of this process. We strongly encourage the use of tab routing rather than jump scoring.</p>
			<p>Scoring cannot be stopped precisely enough to end a score line like a CNC route. To make a complete score, the scoring blade must travel past the end point and come to a stop. We require a minimum of a 0.25" (6.35mm) gap between the stop score location and anything that is not intended to be scored.</p>
			<strong>Tab Routed Arrays</strong>
			<p>
				<img src="http://via.placeholder.com/275x345" align="left" alt="Image alt tag here" class="pad-right">If your design has an irregular shape, then tab routing may be required. Our default is to add a 0.1" (2.54mm) gap between the boards to allow the router bit to pass between them. Small tabs of material will remain to hold the boards in place. To make separation easier, we can add small non-plated holes to the tabs called 'mouse bites' to perforate the tab. If you desire a smooth edge on your PCBs, these areas will need to be sanded after the boards are removed from the array.
			</p>
			<p>Because tab routed arrays are inherently less sturdy than scored arrays, we will add a 0.4" (10mm) tooling rail to all four sides of the array. This will give your array more support during handling and assembly processes. (In the above photo there are rails on only two sides per this customer's request.) After the 0.1" router bit passes between the board and the rail, the resulting rail width will be 0.3".</p>
			<strong>Stencils</strong>
			<p>If we set up the array for you, your assembler will need to receive the arrayed Gerber files so a stencil can be made with the exact spacing of the final design. We will be happy to send you the files but please be aware that we also make stencils! Simply contact your sales rep for more information.</p>
			<p>If you have any questions, please
				<a href="pcbu-contact.php">contact us</a>; we will be glad to assist you with your array!</p>
		</div>
	</div>
</article>
<?php include_once('includes/footer.php'); ?>
