<!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PCB PRIME</title>

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
	<meta name="msapplication-TileColor" content="#603cba">
	<meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/app.css">
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.0/slick-theme.css"/>

	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"
            integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe"
            crossorigin="anonymous"></script>

    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
</head>

<body>
<div class="off-canvas-wrapper">
    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper="">
        <div class="off-canvas position-left is-transition-push" id="offCanvas" data-off-canvas="5i0up3-off-canvas"
             data-position="left" aria-hidden="true">
            <ul class="off-canvas-list">
                <li><label class="first">PCB Prime</label></li>
                <li><a href="index.html">Home</a></li>
            </ul>
            <hr>
            <ul class="off-canvas-list">

                <li>
                    <a href="#">Sign In</a>

                </li>
                <li>
                    <a href="#">Register</a>
                </li>
            </ul>
            <hr>

            <ul class="off-canvas-list">
                <li><a href="#">Products</a></li>
                <li><a href="#">Tech tips</a></li>
                <li><a href="#">Faq</a></li>
                <li><a href="#">About</a></li>
                <li><a href="">Contact</a></li>
                <li><a href="">Cart</a></li>
            </ul>
            <hr>
            <ul class="off-canvas-list">
                <li><a href="mailto:info@pcbprime.com" class="header-mail"><i class="fas fa-envelope bright-blue"></i>
                        info@pcbprime.com</a>
                </li>
                <li>&nbsp;</li>
                <li><a href="tel:1808675309" class="header-tel"><span class="bright-blue"><i
                                class="fas fa-phone"></i></span> 800.867.5309</a>
                </li>

            </ul>
            <hr>
            <ul class="off-canvas-list">
                <li class="position-rel"><input type="search" placeholder="Search" class="align-middle">
                    <button type="button" class="button clear bright-blue"><i class="fas fa-search"></i>
                    </button>
                </li>
            </ul>

        </div>
        <div class="off-canvas-content" data-off-canvas-content="">
            <div class="title-bar hide-for-medium">
                <div class="title-bar-left">
                    <button class="menu-icon-style" type="button" data-open="offCanvas" aria-expanded="false"
                            aria-controls="offCanvas"><i class="fas fa-bars" style="color:#fff;"></i>
                        <a class="title-bar-home-link" href="" target="_blank"><span class="title-bar-title"><img
                                    src="images/PCB_Prime_White_V5.svg" alt="PCB Prime" class="mobile-logo"></a>
                    </button>
                </div>
            </div>
            <header class="hero home-large-hero">

                <div class="grid-container util-gradient-bg fluid show-for-medium">
                    <div class="grid-container">
                        <div class="grid-x">
                            <div class="top-bar-right cell" id="utility-menu">
                                <ul class="menu align-right">
                                    <li class="position-rel "><input type="search" placeholder="Search"
                                                                     class="align-middle">
                                        <button type="button" class="button clear bright-blue"><i
                                                    class="fas fa-search bright-blue"></i>
                                        </button>
                                    </li>
                                    <li><a href="mailto:info@pcbprime.com" class="header-mail"><i
                                                    class="fas fa-envelope bright-blue"></i> info@pcbprime.com</a>
                                    </li>
                                    <li class="align-middle">&#9679;</li>
                                    <li><a href="tel:1808675309" class="header-tel"><span class="bright-blue"><i
                                                        class="fas fa-phone"></i></span> 800.867.5309</a>
                                    </li>
                                    <li class="login">
                                        <a href="#" class="align-right">
                                            <small>Sign In</small>
                                        </a>
                                        <a href="#" class="align-left">
                                            <small>Register</small>
                                        </a>
                                    </li>

                                </ul>
                            </div>

                        </div>
                    </div>
                </div>


                <div class="grid-container">
                    <div class="grid-x">
                        <div class="top-bar cell show-for-medium" id="main-nav">
                            <div class="top-bar-left">
                                <ul class="menu align-left text-left">
                                    <li class="topbar-title float-left"><a class="pcb-logo"
                                                                           href="/">PCB<strong>PRIME</strong></a>
                                </ul>
                            </div>
                            <div class="top-bar-right">
                                <nav class="topbar show-for-medium">
                                    <ul class="menu">
                                        <li class="active">
                                            <a href="/">Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Products</a>

                                        </li>
                                        <li>
                                            <a href="/tip-category.php">Tech Tips</a>

                                        </li>
                                        <li>
                                            <a href="#">FAQ</a>

                                        </li>
                                        <li>
                                            <a href="#">About</a>
                                        </li>
                                        <li>
                                            <a href="#">Contact</a>
                                        </li>
                                        <li><a href="#"><i class="fas fa-shopping-cart"></i>
                                                &nbsp;<small class="cart-nav float-right">My Cart<br>(0)Items</small>
                                            </a></li>

                                    </ul>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>