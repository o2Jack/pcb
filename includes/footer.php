<footer class="footer">
    <div class="footer-wrapper">
        <div class="grid-container liquid">

            <div class="grid-x grid-margin-x">
                <div class="cell large-auto footer-col-2">

                    <div class="footer-logo"></div>
                    At PCB Prime we understand your technical requirements are evolving but your basic needs
                    will always remain the same; consistently high quality: dependable lead times, low cost and
                    responsive service. <a href="#">read more ></a> 
                </div>
                <div class="cell large-auto footer-col-2"><p class="h5">location</p>

                    <ul class="fa-ul">
                        <li><span class="fa-li"><i class="fas fa-map-marker-alt"></i></span>333 Street<br>
                            Somecity, Co 80222
                        </li>
                        <li><span class="fa-li"><i class="fas fa-phone"></i></span>P.: (555) 867-5309</li>
                        <li><span class="fa-li"><i class="fas fa-envelope"></i></span>E.: sales@pcbprime.com
                        </li>

                    </ul>


                </div>
                <div class="cell large-auto footer-col-2"><p class="h5">Newsletter</p>
                    <p>Products and Updates</p>
                    <input type="text"><input type="submit" class="button"></input></div>
            </div>
        </div>
        <div>
            <div class="grid-container">
                <div class="grid-x grid-margin-x">
                    &nbsp;
                </div>
            </div>
            <div class="grid-container">
                <div class="grid-x grid-margin-x">
                    <div class="cell large-9 small-11 footer-nav">

                        <a href="#">Home</a>  |  <a href="#">My Account</a>   |  <a
                            href="#">Products</a>   |  <a href="#">Assembly</a>   |  <a href="#">FAQ</a>   |  <a
                            href="#">How to Order</a> <br><a href="#">Glossary</a>   |  <a href="#">Testimonials</a>   |  <a
                            href="#">Privacy</a>   |  <a href="#">Contact Us</a> 
                        © 2018 PCB Prime. All rights reserved.
                    </div>

                    <div class="cell large-3 small-11 social">
                        <a href="https://twitter.com"><i class="fab fa-twitter-square fa-3x"></i></a>
                        <a href="https://facebook.com"><i class="fab fa-facebook fa-3x"></i></a>
                        <a href="https://googleplus.com"><i class="fab fa-google-plus-square fa-3x"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</footer>

</div>
</div>
</div>

<script src="js/vendor/jquery.js"></script>
<script src="js/vendor/what-input.js"></script>
<script src="js/vendor/foundation.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="js/app.js"></script>
</body>
</html>