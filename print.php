<?php include_once('includes/header.php'); ?>
<article class="page-content">
	<div id="printThis">
		<div class="grid-container border padding-collapse">
			<div class="grid-x">
				<div class="large-6 cell custom-flex">
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-6 cell border">
							<img src="images/pcbprime-print-logo.png" alt="PCB Prime Logo"/></div>
						<div class="large-6 cell border">
							<div class="grid-x grid-padding-x grid-padding-y">
								<div class="large-12 cell dark-blue">PCB Prime Headquarters</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y">
								<div class="large-12 cell">
									<br>
									<small>13900 E. Florida Ave | Suite F | Aurora, CO 80012</small>
									<small>
										Contact : Scott Walsh <br> Email : scott@pcbprime.com
										<br> Direct: 303-862-8925 Fax: 303-952-506
									</small>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-3 cell text-right border">
							Company :<br> Contact :<br> Part Number :
						</div>
						<div class="large-9 cell border">
							{{Controltek}}<br> {{Angela Striker}}<br> {{180-2908}}
							<div class="float-right">{{Rev A00}}</div>
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y flex">
						<div class="large-3 cell text-right border">
							Email :<br> Phone :<br> Description :
						</div>
						<div class="large-9 cell border">
							{{angela@controltek}}<br> {{555-555-5555}}<br> {{01300-00200-00018}}
						</div>
					</div>
				</div>
				<div class="large-6 cell custom-flex">
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border"> Quality Service You can Depend On</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-3 cell border">
							Incredible Pricing High Quality Fast Turn Times Even Faster Quotes Aluminum Flex / Rigid Flex
						</div>
						<div class="large-6 cell border custom-flex">
							<div class="grid-padding-x  grid-padding-y">
								<div class="large-12 cell light-blue border">Always Free Tooling and Free Electrical Test!</div>
								<div class="large-12 cell border flex">
									It's easy to move your legacy parts to us. We'll match your stencil so you can start saving money today!
									<br><br><br></div>
							</div>
						</div>
						<div class="large-3 cell border">
							RoHS/REACH Compliant Conflict Mineral Free UL & ISO 9001 Certified Free DFM File Review Prototypes to Full Production
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border">
							Place your order as late as 9pm EST / 6pm PST !
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y flex">
						<div class="large-3 cell border">
							Quote Number<br> Gerber File Name
						</div>
						<div class="large-9 cell border">
							<span class="red-text">{{SW12345-1254}}</span>
							<div class="float-right">Date {{5/15/2018 14:47}}</div>
							<br> <span class="red-text">{{ZINC5_01300-00200-00018_GBR_REV A00_2up.zip}}</span><br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-container padding-collapse border">
			<div class="grid-x">
				<div class="large-6">
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border">Quoted PCB Specifications
						</div>
					</div>
					<div class="grid-x"> <!-- two columns -->
						<div class="large-6 cell">
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									Layers
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									Board Size
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									Array Size
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									Array
								</div>
								<div class="large-6 cell">
									{{Y/N}}
								</div>
								<div class="large-6 cell">
									# In Array
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									X-Outs Allowed
								</div>
								<div class="large-6 cell">
									{{Yes/No}}
									<div style="display:block; height:2px; "><!--Spacer --></div>
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									Scoring
								</div>
								<div class="large-6 cell">
									{{Yes}}
								</div>
								<div class="large-6 cell">
									Jump Scoring
								</div>
								<div class="large-6 cell">
									{{No}}
								</div>
								<div class="large-6 cell">
									Tab Rout
								</div>
								<div class="large-6 cell">
									{{Yes}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Material
								</div>
								<div class="large-7 cell">
									{{FR-4 (170°C Tg}}
								</div>
								<div class="large-5 cell">
									Final Thickness
								</div>
								<div class="large-7 cell">
									{{0.062in / 1.6mm}}
								</div>
								<div class="large-5 cell">
									Plating
								</div>
								<div class="large-7 cell">
									{{Immersion Gold}}
								</div>
								<div class="large-5 cell">
									Outer Cu Weigh
								</div>
								<div class="large-7 cell">
									{{2}}
								</div>
								<div class="large-5 cell">
									Inner Cu Weigh
								</div>
								<div class="large-7 cell">
									{{2}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Sides Mask
								</div>
								<div class="large-7 cell">
									{{Both}}
								</div>
								<div class="large-5 cell">
									Mask Color
								</div>
								<div class="large-7 cell">
									{{Matte Blue}}
								</div>
								<div class="large-5 cell">
									Sides Silkscreen
								</div>
								<div class="large-7 cell">
									{{Both}}
								</div>
								<div class="large-5 cell">
									Silkscreen Color
								</div>
								<div class="large-7 cell">
									{{Silkscreen Color}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Gold Fingers
								</div>
								<div class="large-7 cell">
									{{Yes #}}
								</div>
								<div class="large-5 cell">
									Gold Thicknes
								</div>
								<div class="large-7 cell">
									{{30u}}
								</div>
								<div class="large-5 cell">
									Bevel Angle
								</div>
								<div class="large-7 cell">
									{{30}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Min Trace/Space
								</div>
								<div class="large-7 cell">
									{{7mil / 0.18mm}}
								</div>
								<div class="large-5 cell">
									Min Hole Size
								</div>
								<div class="large-7 cell">
									{{10mil / 0.25mm}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-8 cell">
									# Holes Under 12 mil
								</div>
								<div class="large-4 cell">
									{{4}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									Plated Slots
								</div>
								<div class="large-6 cell">
									{{Yes}}{{#}}
								</div>
								<div class="large-6 cell">
									Plated Edges
								</div>
								<div class="large-6 cell">
									{{Yes}}{{#}}
								</div>
							</div>
						</div>
						<div class="large-6 cell custom-flex">
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									RoHS
								</div>
								<div class="large-6 cell">
									{{Yes/No}}
								</div>
								<div class="large-6 cell">
									ITAR
								</div>
								<div class="large-6 cell">
									{{Yes/No}}
								</div>
								<div class="large-6 cell">
									IPC Inspection Class
								</div>
								<div class="large-6 cell">
									{{#}}
								</div>
								<div class="large-6 cell">
								</div>
								<div class="large-6 cell">
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-12 cell text-center">
									UL/Date Code/94V-0 Mark
								</div>
								<div class="large-12 cell text-center">
									Per Fab Print
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-8 cell">
									Controlled Impedance
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Controlled Dielectric
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Carbon Ink
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-8 cell">
									Castellated Holes
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Blind/Buried Vias
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Via in Pad
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Mask Tented Vias
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Epoxy Plugged Vias
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Conductive Filled Vias
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Counter Sinks/Bores
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-12 cell dark-blue">
									Special Notes
								</div>
							</div>
							<div class="flex grid-x grid-padding-x grid-padding-y border light-blue">
								<div class="large-12 cell">
									Notes that can be saved about the quote should be entered here.
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="large-6 cell custom-flex">
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border">Lead times in Business Days (Mon-Fri)
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-12 cell">
							<br>
							<div class="grid-x grid-padding-x grid-padding-y ">
								<div class="large-12 cell border dark-blue">Unit Pricing (Cost per individual board) Qty is per board not per array
								</div>
								<div class="large-12 border">
									<table class="pricing-table">
										<thead>
										<tr>
											<th>Qty</th>
											<th>2 Day</th>
											<th>3 Day</th>
											<th>4 Day</th>
											<th>5 Day</th>
											<th>8 Day</th>
											<th>10 Day</th>
											<th>15 Day</th>
											<th>20 Day</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										</tbody>
									</table>
								</div>
								<div class="large-12 cell border dark-blue">
									Lot Pricing (Unit Qty x Unit Price = Lot Price) Qty is per board not per array
								</div>
								<div class="large-12 border">
									<table class="pricing-table">
										<thead>
										<tr>
											<th>Qty</th>
											<th>2 Day</th>
											<th>3 Day</th>
											<th>4 Day</th>
											<th>5 Day</th>
											<th>8 Day</th>
											<th>10 Day</th>
											<th>15 Day</th>
											<th>20 Day</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										</tbody>
									</table>
									<div class="large-12 cell">
										<div class="float-left"> Tooling NRE:
											<span style="text-decoration:line-through" class="red-text">$300</span>
										</div>
										<div class="float-right">100% Electrical Test:
											<span style="text-decoration:line-through" class="red-text">$300</span>
										</div>
									</div>
								</div>
								<div class="large-12 cell attention border">
									<span class="red-text lead">Free Tooling and Electrical Test</span><br> Free DFM (Design For Manufacturing) Review on Every Order
								</div>
								<div class="large-12 cell light-blue border">
									<h5 class="red-text">Ordering is Easy</h5>
									To place an order, just call, email or fax me. I will be happy to place your order for you. My direct contact information is below.<br>
									<div class="red-text"> Email :
										<a href="mailto:" scott@pcbprime.com" ">scott@pcbprime.com</a>
										<br>Direct : <a href="tel:3038628925">303-862-8925</a> <br>Fax : 303-952-5063
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y border flex">
						<div class="large-12 cell">
							<ul>
								<li>Quote valid for 30 days.</li>
								<li>Daily order cut off time is 9pm EST / 6pm PST. Day zero begins once files are approved by engineering.</li>
								<li>Costs and lead times do not include shipping. Standard UPS Domestic Rates and delivery times apply. FOB Aurora, CO USA.</li>
								<li class="red-text">If certain specs were not specifically stated in your files, we assumed our industry standard specs. If any specs are inaccurate, let us know immediately. It may not only affect the cost but also how your boards are constructed !</li>
							</ul>
							<div class="float-right">0 v-Prime 18.05.15</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<br><br><br>
</article>
<?php include_once('includes/footer.php'); ?>
