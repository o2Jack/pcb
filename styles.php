<?php include_once('includes/header.php'); ?>


<article class="page-content">
	<div class="grid-container">
		<div class="large-12">
			<p class="lead">Lead in teaxt paragraph style is poppins 20px 32px line height</p>


			<h2 class="bottom-line">H2 with line option 40px</h2>


			<p class="page-intro">A <a href="learn/about.html">Framework</a> for any device, medium, and
				accessibility. Foundation is a family of responsive front-end frameworks that make it easy
				to design beautiful responsive websites, apps and emails that look amazing on any device.
				Foundation is semantic, readable, flexible, and completely customizable. We’re constantly
				adding new resources and <a href="https://foundation.zurb.com/building-blocks/">code
					snippets</a>, including these handy <a href="templates.html">HTML templates</a> to help
				get you started!</p>

			<h2>h2. This is a large header.</h2>
			<h3>h3. This is a medium header.</h3>
			<h4>h4. This is a moderate header.</h4>
			<h5>h5. This is a small header.</h5>
			<h6>h6. This is a tiny header.</h6>


			<h2 class="subheader">h2.subheader</h2>
			<h3 class="subheader">h3.subheader</h3>
			<h4 class="subheader">h4.subheader</h4>
			<h5 class="subheader">h5.subheader</h5>
			<h6 class="subheader">h6.subheader</h6>

			<ul>
				<li>List item with a much longer description or more content.</li>
				<li>List item</li>
				<li>List item
					<ul>
						<li>Nested list item</li>
						<li>Nested list item</li>
						<li>Nested list item</li>
					</ul>
				</li>
				<li>List item</li>
				<li>List item</li>
				<li>List item</li>
			</ul>

			<ol>
				<li>Cheese (essential)</li>
				<li>Pepperoni</li>
				<li>Bacon
					<ol>
						<li>Normal bacon</li>
						<li>Canadian bacon</li>
					</ol>
				</li>
				<li>Sausage</li>
				<li>Onions</li>
				<li>Mushrooms</li>
			</ol>

			<blockquote>
				Those people who think they know everything are a great annoyance to those of us who do.
				<cite>Isaac Asimov</cite>
			</blockquote>

			<button class='btn dark-more-button' type='submit' value='submit'>
				 Submit <i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i>
			</button>


		</div>
	</div>
</article>


<?php include_once('includes/footer.php'); ?>
