<?php include_once('includes/header.php'); ?>
<article class="page-content">
	<div class="grid-container fluid">
		<div class="large-12">
			<ul class="breadcrumb-counter-nav">
				<li class="breadcrumb-counter-nav-item current"><a href="#">Quote</a></li>
				<li class="breadcrumb-counter-nav-item"><a href="#">File Upload</a></li>
				<li class="breadcrumb-counter-nav-item"><a href="#">Shipping</a></li>
				<li class="breadcrumb-counter-nav-item"><a href="#">Payment</a></li>
				<li class="breadcrumb-counter-nav-item"><a href="#">Review</a></li>
			</ul>
			<form>
				<div class="grid-container fluid">
					<div class="grid-x grid-padding-x ">
						<div class="large-3 medium-6">
							<div class="card left-form-section">
								<div class="card-divider">
									Basic Specifications
								</div>
								<div class="card-section">
									<label>Quantity (enter up to 5 values)<span data-tooltip data-allow-html="true"
												class="top help"
												tabindex="1"
												title="<strong><u>Quantity</u></strong><br /><br />Up to 5 different quantities can be entered.<br><br>Tip: Press TAB after each qty to quickly jump to the next available cell."><i class="fas fa-question-circle"></i></span></label>
									<div class="grid-container">
										<div class="grid-x">
											<div class="medium-2 small-padding-rt cell">
												<input type="text"/>
											</div>
											<div class="medium-2 small-padding-rt cell">
												<input type="text"/>
											</div>
											<div class="medium-2 small-padding-rt cell">
												<input type="text"/>
											</div>
											<div class="medium-2 small-padding-rt cell">
												<input type="text"/>
											</div>
											<div class="medium-2 small-padding-rt cell">
												<input type="text"/>
											</div>
										</div>
									</div>

									<div class="grid-x grid-padding-x">
										<div class="large-6 cell">
										<label>Number of Copper Layers
											<span data-tooltip class="top help" data-allow-html="true" tabindex="2" title="<strong><u>Number of Copper Layers</u></strong><br /><br />The total number of copper layers in your design.<br><br>Tip: Try to design using an even number of layers (2, 4, 6, 8, etc.). Because the materials used in the manufacturing process already have copper on both sides, the cost for a 3-layer board is the same as for a 4-layer board, for example. Additionally, there can be warping concerns with boards that have odd numbered layer construction due to unbalanced copper distribution."><i class="fas fa-question-circle"></i></span>
											<input type="number" name="layers" id="layers" value="0" min="1" max="36"></label>
										</div>
										<div class="large-6 cell">
											<label>Is your design
												ITAR controlled?											<span data-tooltip class="top help" data-allow-html="true" tabindex="2" title="<strong><u>ITAR</u></strong><br /><br />ITAR Restricted<br /><br />International Traffic in Arms Regulations. This is a US Government regulation that prohibits the export of (usually) military designs for manufacture outside the United States or to any company that that employs non-US citizens. Most designs will not have ITAR restrictions. By selecting ‘No’, you are declaring that your design is not ITAR controlled.<br><br>Unfortunately we are not currently accepting ITAR orders. We are always adding to our services so please check back with us in the future."><i class="fas fa-question-circle"></i></span>
											</label>
											<label for="itarNo" class="float-left">No </label>&nbsp; <input type="radio" name="itar" class="aright" id="itarNo"  value="0">
											<br clear="all"/>
											<label for="itarYes" class="float-left">Yes </label>&nbsp; <input type="radio" name="itar" class="aright" id="itarYes"  value="1">

										</div>
										<div class="large-6 cell">
											<label>Board Size
												<span data-tooltip class="top help" data-allow-html="true" tabindex="3" title="<strong><u>Dimensions</u></strong><br /><br />Enter the overall dimensions for your board. Measure the total length and width along the X and Y axes.<br><br>If your board is a single design and is step-and-repeated in an array, enter the size of a single board. Select the ARRAY check box in the section below and enter the overall size of the desired array, including any tooling rails, in the ARRAY SIZE area.<br><br>Additionally, when ordering an array, you must select at least one scoring/routing method: Scoring, Jump Scoring, or Tab-routing."><i class="fas fa-question-circle"></i></span></label>
											<div class="grid-container">
												<div class="grid-x">
													<div class="medium-5 small-padding-rt cell">
														<input name="displayX" type="text" id="displayX" class="" value="">
													</div>
													<div class="medium-1 cell text-center align-middle">
														X
													</div>
													<div class="medium-5 small-padding-rt cell">
														<input name="displayY" type="text" id="displayY" class="" value="">
													</div>
												</div>
											</div>
										</div>
										<div class="large-6 cell">

													<label>Measurement Units <select>
															<option value="inches">Inches</option>
															<option value="mm">mm</option>
														</select> </label>

										</div>
									</div>
									<hr>

										<fieldset>
											<div class="grid-x grid-margin-x">
											<div class="large-6 cell">
											<label> Material
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Material</u></strong><br /><strong>Default: FR4 (135°C Tg)</strong><br /><br />Tip: All of our PCB laminates are RoHS compliant. RoHS compliancy is not an indication of a laminate’s temperature requirements or a specific final finish. Consult with your PCB assembler if you are unsure what final finish or laminate they require for their RoHS assembly process."><i class="fas fa-question-circle"></i></span>
											</label> <select name="material" id="material" class="">
												<option value="2" class="" selected="">FR4 (135°C Tg)</option>
												<option value="12" class="">FR4 (150°C Tg)</option>
												<option value="3" class="">FR4 (170°C Tg)</option>
												<option value="13" class="">FR4 (180°C Tg)</option>
												<option value="15" class="">Aluminum 1 w/m.k</option>
												<option value="16" class="">Aluminum 2 w/m.k</option>
												<option value="17" class="">Aluminum 3 w/m.k</option>
												<option value="18" class="">Aluminum 1 w/m.k (ITEQ F859)</option>
												<option value="6" class="">Flex/Polyimide</option>
												<option value="14" class="">Rigid/Flex</option>
												<option value="8" class="">Rogers 4350</option>
												</select>
											</div>
											<div class="large-6 cell">
											 <label> Final Thickness
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Final Thickness</u></strong><br /><strong>Default: 0.062 in (1.6mm)</strong><br /><br />This is the overall finished thickness of your PCB.<br /><br />Tip: Thicknesses equal to .020 in (.5mm) or less cannot have a solder finish plating. The solder application process will damage thin laminates. The recommended finish for thin laminates is ENIG (immersion gold)."><i class="fas fa-question-circle"></i></span>
											</label> <select name="thickness" id="thickness" class="">
												<option value="0.020" class="">0.020 inch</option>
												<option value="0.024" class="">0.024 inch</option>
												<option value="0.031" class="">0.031 inch</option>
												<option value="0.040" class="">0.040 inch</option>
												<option value="0.047" class="">0.047 inch</option>
												<option value="0.062" class="" selected="">0.062 inch</option>
												<option value="0.070" class="">0.070 inch</option>
												<option value="0.080" class="">0.080 inch</option>
												<option value="0.093" class="">0.093 inch</option>
												<option value="0.100" class="">0.100 inch</option>
												<option value="0.125" class="">0.125 inch</option>
												<option value="0.150" class="">0.150 inch</option>
												<option value="0.200" class="">0.200 inch</option>

											</select>

											</div>
												<div class="large-6 cell">
												<label> Plating
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Final Plating</u></strong><br /><strong>Default: Lead Solder</strong><br /><br />This is sometimes referred to as the final finish. It is the plating that will be applied to all outer layer copper features that are exposed in the solder mask.<br /><br />Tip: The only finish we offer that is NOT RoHS compliant is Lead Solder. All other finishes we offer are fully RoHS compliant."><i class="fas fa-question-circle"></i></span>
											</label> <select name="plating" id="plating" class="">
												<option value="1" class="">No Copper</option>
												<option value="2" class="">Bare Copper</option>
												<option value="3" class="">Lead Solder</option>
												<option value="4" class="" selected="">Lead-free Solder</option>
												<option value="5" class="">Immersion Gold</option>
												<option value="6" class="">Immersion Silver</option>
												<option value="8" class="">OSP (Entek)</option>
												<option value="9" class="">Deep/Hard Gold</option>
												<option value="11" class="">Wirebondable Gold</option>
											</select>
												</div>
												<div class="large-6 cell">
													 RoHS<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>RoHS</u></strong><br /><br />RoHS is a European Directive that took effect July 1, 2006 that restricts the use of six hazardous materials in the manufacture of various types of electronic equipment. The six materials are:<br><br>1. Lead (Pb)<br>2. Mercury (Hg)<br>3. Cadmium (Cd)<br>4. Hexavalent Chromium (Cr6+)<br>5. Polybrominated biphenyls (PBB)<br>6. Polybrominated diphenyl ether (PBDE)<br><br>We do not use any of these elements or compounds in our materials or processing with the exception of lead. The only way lead will be used is if lead solder is selected as your final finish. All other options we offer are RoHS compliant.<br><br>Be aware that RoHS compliance is not an indication of your laminate’s temperature rating. Some assemblers require a laminate with a higher temperature rating due to their particular lead free process. Consult with your assembler if you are unsure."><i class="fas fa-question-circle"></i></span>
												</div>


												<div class="large-12 cell">
													<hr>
												</div>

												<div class="large-6 cell">

													<label>Outer Cu weight
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Outer Copper</u></strong><br /><strong>Default: 1 oz</strong><br /><br />Finished outer layer copper weight after copper plating. (1 oz = 0.00137  thickness)<br /><br"><i class="fas fa-question-circle"></i></span>
											</label> <select name="outerCu" id="outerCu" class="">
												<option value="0.500" class="">0.5 oz</option>
												<option value="1.000" class="" selected="">1 oz</option>
												<option value="1.500" class="">1.5 oz</option>
												<option value="2.000" class="">2 oz</option>
												<option value="3.000" class="">3 oz</option>
												<option value="4.000" class="">4 oz</option>
												<option value="5.000" class="">5 oz</option>
											</select>
												</div>
												<div class="large-6 cell">

													<label>Inner Cu weight
														<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Inner Copper</u></strong><br /><strong>Default: 1 oz</strong><br /><br />Inner layer copper weight. Because inner layers do not receive additional plating like outer layers do, they start and finish with the same copper weight. (1 oz = 1.37 mil = .035 mm)"><i class="fas fa-question-circle"></i></span>
													</label> <select name="outerCu" id="outerCu" class="">
														<option value="0.500" class="">0.5 oz</option>
														<option value="1.000" class="" selected="">1 oz</option>
														<option value="1.500" class="">1.5 oz</option>
														<option value="2.000" class="">2 oz</option>
														<option value="3.000" class="">3 oz</option>
														<option value="4.000" class="">4 oz</option>
														<option value="5.000" class="">5 oz</option>
													</select>
												</div>
												<div class="large-6 cell">
													<label> Solder Mask
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Solder Mask</u></strong><br /><strong>Default: Both sides</strong><br /><br />AKA Solder Resist, the protective coating that prevents your final plating from being applied to all outer layer copper features.<br /><br />Typically mask is applied to both sides of the board and we use industry standard LPI (Liquid Photo Imagable) Class III epoxy based solder mask. The only places on the PCB that will not be covered with mask are defined in your solder mask files.<br><br>Tip: If your design appears to be all thru-hole and we receive only 1 mask file, we will assume it is to be used on both sides.<br>"><i class="fas fa-question-circle"></i></span>
											</label> <select name="solderMask" id="solderMask" class="">
												<option value="0" class="">None</option>
												<option value="1" class="">Bottom</option>
												<option value="2" class="">Top</option>
												<option value="3" class="" selected="">Both Sides</option>
											</select>
												</div>

												<div class="large-6 cell">
												<label>Mask Color
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Mask Color</u></strong><br /><strong>Default: Green</strong><br /><br />The color of your board where solder mask is applied."><i class="fas fa-question-circle"></i></span>
											</label> <select name="maskColor" id="maskColor" class="">
												<option value="3" class="">Black</option>
												<option value="5" class="">Blue</option>
												<option value="7" class="">Bright White</option>
												<option value="1" class="" selected="">Green</option>
												<option value="4" class="">Matte Black</option>
												<option value="10" class="">Matte Blue</option>
												<option value="2" class="">Matte Green</option>
												<option value="8" class="">Red</option>
												<option value="9" class="">White</option>
												<option value="6" class="">Yellow</option>
											</select>
												</div>
												<div class="large-6 cell">
													<label> Silkscreen
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Silkscreen</u></strong><br /><br />AKA Nomenclature<br /><br />Choose which sides of your design will have silkscreen printed in epoxy ink. Normally used for reference designators, graphics and part numbers.<br><br>Tip #1: We will use all silkscreen files included in your data. Do not send a silkscreen file if you do not want it to be used.<br><br>Tip #2: As part of the tooling process, any silkscreen legend that covers pads or annular rings will be clipped to ensure proper final finish adhesion.<br>"><i class="fas fa-question-circle"></i></span>
											</label> <select name="silkscreen" id="silkscreen" class="">
												<option value="0" class="">None</option>
												<option value="1" class="">Bottom</option>
												<option value="2" class="">Top</option>
												<option value="3" class="" selected="">Both Sides</option>
													</select></div>
												<div class="large-6 cell">
													<label> Silkscreen Color
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Silkscreen Color</u></strong><br /><strong>Default: White</strong><br /><br />"><i class="fas fa-question-circle"></i></span>
											</label> <select name="silkColor" id="silkColor" class="">
												<option value="4" class="">Black</option>
												<option value="3" class="">Red</option>
												<option value="1" class="" selected="">White</option>
												<option value="2" class="">Yellow</option>
											</select>
												</div>
												<div class="larger-12 cell">
													<hr>
												</div>

												<div class="large-6 cell">
													<label for="bevelAngle"> Min. Trace/Space
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Minimum Traces & Spaces</u></strong><br /><br />Minimum trace width and spacing between copper features, whichever is least. The copper thickness you select will determine how narrow a trace or space can be manufactured. This quote form will not allow you to quote a trace or space that is too narrow for your selected copper weight. The thicker the copper, the wider the traces and spaces need to be. "><i class="fas fa-question-circle"></i></span>
											</label><select name="minTraceSpace" id="minTraceSpace" class="">
												<option value="3" class="">0.003 inch</option>
												<option value="4" class="">0.004 inch</option>
												<option value="5" class="">0.005 inch</option>
												<option value="6" class="">0.006 inch</option>
												<option value="7" class="">0.007 inch</option>
												<option value="8" class="" selected="">0.008 inch</option>
												<option value="9" class="">0.009 inch</option>
												<option value="10" class="">0.010 inch</option>
												<option value="11" class="">0.011 inch</option>
												<option value="12" class="">0.012 inch</option>
												<option value="13" class="">0.013 inch</option>
												<option value="14" class="">0.014 inch or greater</option>
													</select></div>
												<div class="large-6 cell">
													<label for="bevelAngle"> Min. Hole Size
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Minimum Hole Size</u></strong><br /><br />The diameter of the smallest finished hole on your board.<br><br>Tip: The cost is the same for holes that are .012 in (.3mm) and larger. Holes between .008 in and .011 in add a small charge. We can drill holes smaller than .008 in but this requires the use of a laser and will add a premium and must be custom quoted. "><i class="fas fa-question-circle"></i></span>
											</label> <select name="minHoleSize" id="minHoleSize" class="">
												<option value="4" class="">.004 inch</option>
												<option value="5" class="">.005 inch</option>
												<option value="6" class="">.006 inch</option>
												<option value="7" class="">.007 inch</option>
												<option value="8" class="">.008 inch</option>
												<option value="9" class="">.009 inch</option>
												<option value="10" class="">.010 inch</option>
												<option value="11" class="">.011 inch</option>
												<option value="12" class="" selected="">.012 inch</option>
												<option value="13" class="">.013 inch</option>
												<option value="14" class="">.014 inch</option>
												<option value="15" class="">.015 inch</option>
												<option value="16" class="">.016 inch</option>
												<option value="17" class="">.017 inch</option>
												<option value="18" class="">.018 inch</option>
												<option value="19" class="">.019 inch</option>
												<option value="20" class="">.020" or greater</option>
											</select>
												</div>
												<div class="large-6 cell">
													<label for="bevelAngle"> Qty. Holes < 12 mil
												<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Qty of Holes < 12 mil</u></strong><br /><strong>Default: 0</strong><br /><br />What is the total number of drill hits on your board where the finished hole size is less than .012 in (.3 mm)?"><i class="fas fa-question-circle"></i></span>
											</label> <input name="holesLT12mil" id="holesLT12mil" type="text" value="0">
												</div>
											</div>
										</fieldset>
								</div>
							</div>
						</div>
						<div class="large-4 medium-6">
							<div class="card">
								<div class="card-divider">
									Arrays & Routing
								</div>
								<div class="card-section">

										<fieldset>
											<div class="grid-x grid-margin-x">
											<div class="large-12 cell">
											<input id="arays" type="checkbox"><label for="arrays">Array
												<span data-tooltip class="top help" data-allow-html="true" tabindex="4" title="<strong><u>Array</u></strong><br><br>Enter your overall array dimensions in the ARRAY SIZE fields to calculate an accurate quote. The array size is the total size of the array. This includes any tooling rails and spacing between the boards, if there are any. If you are not sure what that size will be, contact us, we are glad to help!<br><br>There are several options for setting up your array.<ol><li>You can step-and-repeat the design yourself</li><li>We can follow your instructions on how the single image should be panelized</li><li>We can send you a suggested array PDF that you can send to your assembler for approval."><i class="fas fa-question-circle"></i></span></label>
											</div>



												<div class="large-6 cell">
												<label>Array Size Inches
													<span data-tooltip class="top help" tabindex="5" data-allow-html="true" title="<strong><u>Array Dimensions</u></strong><br /><br />The total size of your array. This includes any spacing between boards and any tooling rails."><i class="fas fa-question-circle"></i></span></label>
													<div class="grid-container">
													<div class="grid-x">
													<div class="medium-5 small-padding-rt cell">
														<input name="displayArX" type="text" id="displayArX" class="" value="">
													</div>
													<div class="medium-1 cell text-center">
														X
													</div>
													<div class="medium-5 cell">
														<input name="displayArY" type="text" id="displayArY" class="" value="">
													</div>
													</div>
												</div>
												</div>
												<div class="large-6 cell">
												<label> Number of Parts/Array
													<span data-tooltip class="top help" tabindex="6" data-allow-html="true" title="<strong><u>Number of Parts per Array</u></strong><br /><br />The number of times the exact same board will be step-and-repeated in the array. This only applies if it is the exact same design being repeated.<br /><br /><u>If you have more than ONE unique design in your array, do not</u> select an array. Instead, enter the overall dimensions for the entire group of boards, including any tooling rails, into the individual board size area. We have to account for the total size of the design. By doing this, you will be quoting the entire group of designs as one piece. When entering quantities to quote, you would enter the number of copies of the entire design you need.<ol><li>Do not select ARRAY.</li><li>Enter the overall size at the very top of the quote form.</li><li>Enter the number of unique designs (underneath the Tab-Routing option).</li><li>Select your method of routing (Scoring, Tab-Routed, Individually Routed, etc.).</li></ol>"><i class="fas fa-question-circle"></i></span></label>                                                </label>
												<input name="brdsInArray" id="brdsInArray" type="text" value="">

												</div>
												<div class="large-6 cell">
													<label> X-Outs Allowed
													<span data-tooltip class="top help" tabindex="7" data-allow-html="true" title="<strong><u>X-Outs Allowed?</u></strong><br /><strong>Default: Yes</strong><br /><br />When manufacturing an array of boards, sometimes a small percentage of the arrays will have some boards that either fail electrical testing or fail final QC inspection for one reason or another. It is customary to ‘X-Out’ these boards in the array rather than scrap the entire array. They will be clearly marked with a black ‘X’. If you or your assembler choose not to process arrays with some boards ‘X-ed Out’, select NO for this option. This will raise the bare board manufacturing cost since more arrays will need to be manufactured to ensure there will be enough 100% good arrays to complete your order."><i class="fas fa-question-circle"></i></span></label>                                                </label>
												<select name="xOuts" id="xOuts">
													<option value="1" selected="">YES</option>
													<option value="0">No</option>
												</select></div>
												<div class="large-6 cell">
													<br clear="all"/>
												<input id="scoring" type="checkbox"><label for="scoring">Scoring
													<span data-tooltip class="top help" data-allow-html="true" tabindex="8" title="<strong><u>Scoring</u></strong><br /><br />If your boards have straight sides, scoring is the best option for your array. Typically the score blade depth will be set to 1/3 the total thickness of the board and we will score the board outline on the top and bottom of the PCB. The amount of remaining material will be approximately 1/3 of the total thickness of the PCB. This scoring option can only be performed vertically or horizontally all the way across the array. If your board has an irregular shape then tab-routing might be required. Arrays can have a mixture of scoring and tab-routing. Consult with your sales rep if you have questions about the best way to setup your array."><i class="fas fa-question-circle"></i></span></label>
													<br clear="all"/>
													<input id="jumpScoring" type="checkbox"><label for="jumpScoring">Jump Scoring
													<span data-tooltip class="top help" data-allow-html="true" tabindex="9" title="<strong><u>Jump Scoring</u></strong><br /><strong>Default: No</strong><br /><br />Similar to standard scoring but this option can stop and start rather than having to continuously score across the entire array.<br /><br />This method is not recommended due to the increased time, cost and restrictions on how this particular option can be utilized.<br><br>The score blade cannot be stopped as precisely as a CNC rout so we will need to score past the point where it is expected to stop. We require a min buffer area of .5 in (12.7 mm) from the point where the score is needed to stop. Do not place any features within .5in of the stop score point since they may be scored in this process. Typically this option will need to be reviewed by engineering to make sure there are no manufacturability or cost concerns."><i class="fas fa-question-circle"></i></span></label>
													<br clear="all"/>
													<input id="tabRouting" type="checkbox"><label for="tabRouting">Tab Routing
													<span data-tooltip class="top help" data-allow-html="true" tabindex="10" title="<strong><u>Tab Routing</u></strong><br /><br />Generally used when designs are in an array but they have an irregular shape and scoring is not an option. Typically a .1 in (2.54mm) gap is placed between the boards to allow the router bit to pass between them. Small tabs will be left which hold the parts within the array. The tabs can be pre-drilled with non-plated holes called mouse bites. These mouse bites will make the tabs easier to snip and sand after de-panelizing. It may make sense for some arrays to utilize a combination of scoring and tab-routing. Consult with your sales rep if you have questions."><i class="fas fa-question-circle"></i></span></label>

												</div>
												<div class="large-12 cell">
													<hr>
												</div>

												<div class="large-6 cell">
													<label> Number of Unique Designs
													<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Number of Unique Designs</u></strong><br /><strong>Default: 1</strong><br /><br />If your Gerber contains more than 1 unique design, please enter the number of designs here.<br /><br /><u>If you have more than ONE unique design in your array, do not</u> select an array. Instead, enter the overall dimensions for the entire group of boards, including any tooling rails, into the individual board size area. We have to account for the total size of the design. By doing this, you will be quoting the entire group of designs as one piece. When entering quantities to quote, you would enter the number of copies of the entire design you need.<ol><li>Do not select ARRAY.</li><li>Enter the overall size at the very top of the quote form.</li><li>Enter the number of unique designs (underneath the Tab-Routing option).</li><li>Select your method of routing (Scoring, Tab-Routed, Individually Routed, etc.).</li></ol>"><i class="fas fa-question-circle"></i></span></label>

												<input name="numUnique" id="numUnique" type="number" min="1" max="10" value="1" ;">

												</div>
												<div class="large-6 cell">
													<input name="indivRoute" id="indivRoute" type="checkbox" value="1"><label for="indivRoute">Individually Routed
													<span data-tooltip class="top help" data-allow-html="true" tabindex="12" title="<strong><u>Individually Routed</u></strong><br /><strong>Default: no</strong><br /><br />If your layout contains more than 1 unique design and you would like these individually routed, select this option. We will route each part individually and wrap them separately."><i class="fas fa-question-circle"></i></span></label>
<br clear="all"/>
													<input name="routePerim" id="routePerim" type="checkbox" value="1"><label for="routePerim">Route Perimeter ONLY
													<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Route Perimeter ONLY</u></strong><br /><strong>Default: no</strong><br /><br />"><i class="fas fa-question-circle"></i></span></label>
												</div>
												</div>
											</div>
										</fieldset>


							</div>
							<div class="card">
								<div class="card-divider">
									Miscellaneous Options
								</div>
								<div class="card-section">

										<fieldset>
											<div class="grid-x grid-margin-x">
												<div class="large-6 cell">
											<label> Inspection Class
												<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Inspection Class</u></strong><br /><strong>Default: Class 2</strong><br /><br /><strong>Class 3<br/>High Performance Electronic Products</strong><br/>Includes products where continued high performance or performance-on-demand is critical, equipment downtime cannot be tolerated, end-use environment may be uncommonly harsh, and the equipment must function when required, such as life support, aerospace, or other critical systems. <strong><em>We do offer Class 3 construction and inspection. </em></strong><br/><br/><strong>Class 2<br/>Dedicated Service Electronic Products</strong><br/>Includes products where continued performance and extended life is required, and for which uninterrupted service is desired but not critical. Typically, the end-use environment would not cause failures. <strong><em>This is our minimum manufacturing and inspection standard. </em></strong><br/><br/><strong>Class 1<br/>General Electronic Products</strong><br/>Includes products suitable for applications where the major requirement is function of the completed assembly. <strong><em>We do not build boards that meet only Class 1 requirements. </em></strong>"><i class="fas fa-question-circle"></i></span></label>
											<select name="inspectionClass" id="inspectionClass" class="">
												<option value="2" class="" selected="">Class 2</option>
												<option value="3" class="">Class 3</option>
											</select></div>
												<div class="large-6 cell">


													<label>UL Information
												<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>UL Info Placement</u></strong><br /><br />Choose where on your design we should place the UL manufacturing information/date code/94V-0 flame rating."><i class="fas fa-question-circle"></i></span></label>
											<select name="ulInfoPlace" id="ulInfoPlace" class="">
												<option value="0" class="">N/A</option>
												<option value="1" class="" selected="">Best Available Location</option>
												<option value="2" class="">Top Copper</option>
												<option value="3" class="">Top Silk</option>
												<option value="4" class="">Bottom Copper</option>
												<option value="5" class="">Bottom Silk</option>
												<option value="6" class="">Refer to Fab Print</option>
											</select></div>
												<div class="large-12 cell">
												<hr>
												</div>
												<div class="large-6 cell">


													<input name="controlledDie" id="controlledDie" type="checkbox" value="1">
													<label for="controlledDie">Controlled Dielectric
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Controlled Dielectric</u></strong><br /><br />Check this option if you need us to modify your design to meet and test for a desired impedance. If you have already made the calculations, determined the ideal dielectric thicknesses and trace widths to be used, and do not require an impedance verification test, select only Controlled Dielectric."><i class="fas fa-question-circle"></i></span>
													</label>

													<br clear="all"/>

													<input name="controlledImp" id="controlledImp" type="checkbox" value="1">
													<label for="controlledImp">Controlled Impedance
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Controlled Impedance</u></strong><br /><br />Check this option if you need us to modify your design to meet and test for a desired impedance. If you have already made the calculations, determined the ideal dielectric thicknesses and trace widths to be used, and do not require an impedance verification test, select only Controlled Dielectric."><i class="fas fa-question-circle"></i></span>
													</label>

													<br clear="all"/>
													<input name="carbonInk" id="carbonInk" type="checkbox" value="1">
													<label for="carbonInk">Carbon Ink
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Carbon Ink</u></strong><br /><br />Process used for applications that require repeated physical stresses such as keyboards, keypads or even edge connectors."><i class="fas fa-question-circle"></i></span>
													</label>
													<br clear="all"/>
													<input name="platedSlots" id="platedSlots" type="checkbox" value="1">
													<label for="platedSlots">Plated Slots
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Plated Slots</u></strong><br /><br />A plated slot is an internal cut out or slot that has copper plating along the inner walls of the slot.<br><br>Tip: Like plated through holes, a slot cannot be plated unless there is a copper pad surrounding the slot on the top and bottom sides of the board. Without copper surrounding the slot, the electrolytic plating will not plate the inner walls of the slot."><i class="fas fa-question-circle"></i></span>
													</label>
													<br clear="all"/>

													<input name="blindVias" id="blindVias" type="checkbox" value="1">
													<label for="blindVias">Blind/Buried Vias
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Blind/Buried Vias</u></strong><br /><br />Blind Via: A via that is visible from either the top or bottom side of the board but does not pass all the way through the board.<br><br>Buried Via: A via that only connects inner layers and is not visible on either top or bottom side of the board.<br><br>Due to different processing requirements for blind/buried via construction, we will need to review your design before we can determine if your board can be manufactured and at what cost. Please submit your Gerber files to your sales representative for a custom quote."><i class="fas fa-question-circle"></i></span>
													</label>
													<br clear="all"/>
													<input name="tentedVias" id="tentedVias" type="checkbox" value="1">
													<label for="tentedVias">Tented Vias
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Tented Vias</u></strong><br /><br />Tip: If you are intentionally tenting some vias on your board, please make a note of this in your Gerber or readme file. Engineering may question any vias that are missing mask clearances and place your order on Engineering Hold to confirm.<br><br>Tented vias are covered but not filled with solder mask. Depending on the size of the via, a tented via may not remain completely closed. The larger the via the more unlikely it will remain closed. The mask will always cover the copper pad surrounding the hole but plugging or covering the open hole cannot be guaranteed. Mask Plugged vias are vias that are intentionally filled with epoxy (solder mask) by special processing."><i class="fas fa-question-circle"></i></span>
													</label>
													<br clear="all"/>
													<input name="pluggedVias" id="pluggedVias" type="checkbox" value="1">
													<label for="pluggedVias">Mask Plugged Vias
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Mask Plugged Vias</u></strong><br /><br />Vias will be intentionally filled and plugged with mask. Generally used to prevent solder from wicking down and away from the desired location during assembly.<br><br>Tip: Please make a note in your Gerber or readme file this is required and on which vias."><i class="fas fa-question-circle"></i></span>
													</label>
													<br clear="all"/>
													<input name="condFillVias" id="condFillVias" type="checkbox" value="1">
													<label for="condFillVias">Conductive Filled Vias
														<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Conductive Fill Vias</u></strong><br /><br />Vias will be filled with a metallic based fill (usually silver based). Normally used to help conduct heat away from the area.<br><br>Tip: Please make a note in your Gerber or readme file this is required and on which vias."><i class="fas fa-question-circle"></i></span>
													</label>
													 </div>

											<div class="large-6 cell">

											<label for="platedEdges">Plated Edges
												<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Plated Edges</u></strong><br /><br />Plating along the outside edge of the PCB. We need to know if the plating is along 1 or 2 edges."><i class="fas fa-question-circle"></i></span>
											</label> <select name="platedEdges" id="platedEdges" class="">
												<option value="0" class="" selected="">none</option>
												<option value="1" class="">1 edge</option>
												<option value="2" class="">2 edges</option>
												</select>
												<br clear="all"/>
												<label for="countersinks"> Countersinks/bores
													<span data-tooltip class="top help" data-allow-html="true" tabindex="11" title="<strong><u>Countersinks</u></strong><br /><br />Countersinks: A conical shaped hole cut into an object, most commonly used to allow a bolt or screw to sit flush or below the surface of the surrounding material. These holes can be either plated or non-plated. To accurately manufacture these holes, we will need to know the following information.<br><br>1. Which side of the board is the countersink to be drilled? (Top or Bottom)<br>2. What is the angle of the sink?<br>3. What is the diameter of the sink at the surface of the material?<br>4. What is the diameter of the smaller hole (shaft)?<br><br>Counterbores: A cylindrical flat-bottomed hole which enlarges another hole. This is also used to allow a fastener to sit at or below the surrounding surface material. Can be either plated or non-plated.<br><br>1. Which side of the board is the counterbore to be drilled? (Top or Bottom)<br>2. How deep is the bore?<br>3. What is the diameter of the bore at the surface of the material?<br>4. What is the diameter of the smaller hole (shaft)?<br>"><i class="fas fa-question-circle"></i></span>
												</label> <select name="countersinks" id="countersinks" class="">
													<option value="0" class="" selected="">0</option>
													<option value="1" class="">1</option>
													<option value="2" class="">2</option>
													<option value="3" class="">3</option>
													<option value="4" class="">4</option>
													<option value="5" class="">5</option>
													<option value="6" class="">6</option>
													<option value="7" class="">7</option>
													<option value="8" class="">8</option>
													<option value="9" class="">9</option>
													<option value="10" class="">10</option>
													<option value="11" class="">more than 10</option>
												</select></div>
												<div class="large-12 cell">
													<hr>
													<input name="goldFingers" id="goldFingers" type="checkbox" value="1">
													<label for="goldFingers">Gold Fingers
														<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Gold Fingers</u></strong><br /><br />Due to the unstable cost of gold and panelization concerns, orders with gold fingers that are over 1500 total square inches will need to be reviewed by engineering to ensure an accurate quote.<br><br>A low cost option for edge connectors is to plate the fingers with your Final Finish of choice and not bevel the edge rather than selectively plate them with the more expensive Hard Gold and beveling. Choose No Hard Gold on the gold thickness menu and No Bevel on the Bevel Angle. This will give you the lowest cost alternative to test your prototypes before production.<br>"><i class="fas fa-question-circle"></i></span>
													</label> </div>

												<div class="large-6 cell">
													<label for="goldThickness"> Gold Thickness
														<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Gold Thickness</u></strong><br /><strong>Default: 30µ</strong><br /><br /><br>"><i class="fas fa-question-circle"></i></span>
													</label> <select name="goldThickness" id="goldThickness" class="">
														<option value="0" class="" selected="">No Gold</option>
														<option value="10" class="">10µ</option>
														<option value="20" class="">20µ</option>
														<option value="30" class="">30µ</option>
														<option value="40" class="">40µ</option>
														<option value="50" class="">50µ</option>
													</select></div>

												<div class="large-6 cell">
													<label for="bevelAngle"> Bevel Angle
														<span data-tooltip class="top help" data-allow-html="true" tabindex="13" title="<strong><u>Bevel Angle</u></strong><br /><strong>Default: 30°</strong><br /><br /><br>"><i class="fas fa-question-circle"></i></span>
													</label> <select name="bevelAngle" id="bevelAngle" class="">
														<option value="0" class="" selected="">No Bevel</option>
														<option value="20" class="">20°</option>
														<option value="30" class="">30°</option>
														<option value="45" class="">45°</option>
													</select></div>


											</div>
										</fieldset>

								</div>
							</div>

						</div>
						<div class="large-5 medium-12"><div class="grid-container">
								<div class="card">
									<div class="card-divider dark-blue-hdr text-center">
										Lead Times in Business Days (Mon-Fri)
									</div>
									<div class="card-section">
										<table class="pricing-table">
											<thead>
											<tr>
												<th colspan="9" class="dark-blue-hdr">Unit Pricing (Cost per individual board)</th>
											</tr>
											<tr>
												<th>Qty</th>
												<th></th>
												<th>3 Day</th>
												<th>4 Day</th>
												<th>5 Day</th>
												<th>8 Day</th>
												<th>10 Day</th>
												<th>15 Day</th>
												<th>20 Day</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td data-label="Pieces">
													5
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$157.36</td>
												<td data-label="4day">$151.16</td>
												<td data-label="5day">$108.61</td>
												<td data-label="8day">$96.45</td>
												<td data-label="10day">$93.89</td>
												<td data-label="15day">--</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													10
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$76.68</td>
												<td data-label="4day">$75.58</td>
												<td data-label="5day">$54.31</td>
												<td data-label="8day">$48.23</td>
												<td data-label="10day">$46.95</td>
												<td data-label="15day">--</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													25
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$32.74</td>
												<td data-label="4day">$31.21</td>
												<td data-label="5day">$22.42</td>
												<td data-label="8day">$19.94</td>
												<td data-label="10day">$19.39</td>
												<td data-label="15day">--</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													50
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$17.51</td>
												<td data-label="4day">$16.46</td>
												<td data-label="5day">$11.83</td>
												<td data-label="8day">$10.55</td>
												<td data-label="10day">$10.24</td>
												<td data-label="15day">$10.02</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													100
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$10.34</td>
												<td data-label="4day">$9.16</td>
												<td data-label="5day">$6.58</td>
												<td data-label="8day">$5.91</td>
												<td data-label="10day">$5.71</td>
												<td data-label="15day">$5.53</td>
												<td data-label="20day">$5.46</td>
											</tr>
											</tbody>
										</table>
										<div class="large-6"><a class="dark-more-button" href="#">Update
												<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a> Quantities can be changed here, click UPDATE to requote</div>
										<table class="pricing-table">
											<thead>
											<tr>
												<th colspan="9" class="dark-blue-hdr">Lot Pricing (Unit Qty x Unit Price = Lot Price)</th>
											</tr>
											<tr>
												<th>Qty</th>
												<th></th>
												<th>3 Day</th>
												<th>4 Day</th>
												<th>5 Day</th>
												<th>8 Day</th>
												<th>10 Day</th>
												<th>15 Day</th>
												<th>20 Day</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td data-label="Pieces">
													5
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$786.60</td>
												<td data-label="4day">$755.80</td>
												<td data-label="5day">$543.05</td>
												<td data-label="8day">$482.25</td>
												<td data-label="10day">$469.45</td>
												<td data-label="15day">--</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													10
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$786.80</td>
												<td data-label="4day">$755.80</td>
												<td data-label="5day">$543.10</td>
												<td data-label="8day">$482.30</td>
												<td data-label="10day">$469.50</td>
												<td data-label="15day">--</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													25
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$818.50</td>
												<td data-label="4day">$780.00</td>
												<td data-label="5day">$560.50</td>
												<td data-label="8day">$498.50</td>
												<td data-label="10day">$484.75</td>
												<td data-label="15day">--</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													50
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$875.50</td>
												<td data-label="4day">$780.00</td>
												<td data-label="5day">$560.50</td>
												<td data-label="8day">$498.50</td>
												<td data-label="10day">$484.75</td>
												<td data-label="15day">$501.00</td>
												<td data-label="20day">--</td>
											</tr>
											<tr>
												<td data-label="Pieces">
													100
												</td>
												<td data-label="2day"></td>
												<td data-label="3day">$1,034.00</td>
												<td data-label="4day">$916.00</td>
												<td data-label="5day">$658.00</td>
												<td data-label="8day">$591.00</td>
												<td data-label="10day">$571.00</td>
												<td data-label="15day">$553.00</td>
												<td data-label="20day">$546.00</td>
											</tr>
											</tbody>
										</table>
									</div>
									<p class="h5 text-center">CLICK on a PRICE to start your order</p>
									<p class="text-center">
										<small>Prices shown are per piece in $US</small>
									<div class="text-right">Why are some boxes blank <span data-tooltip class="top help" tabindex="5" data-allow-html="true" title="<strong><u>Why are some boxes blank</u></strong><br /><br />Because because because of the wonderful things they does"><i class="fas fa-question-circle"></i></span></div>

									</p>


								</div>




								<div class="card">
									<div class="card-divider attention">
										Free Tooling and Electrical Test !<br>
										Free DFM (Design For Manufacturing) Review on Every Order
									</div>
									<div class="card-section">
										<div class="grid-x grid-margin-x">
											<div class="large-12 cell">
To place an order online, click either the Unit or Lot price that corresponds to the qty and lead time you woul d like to order. if you have a spec or lead time that will not quote online, please contact us, we will be glad to assist you.

											</div>

										</div>
									</div>

								</div>
								Notes
								<textarea rows="4" cols="50" id="notes" name="notes">
</textarea>

								<div class="row grid-x">
									<div class="large-6"><a class="dark-more-button" href="#">Save Note
											<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></div>
									<div class="large-6 text-center"><a class="dark-more-button" data-toggle="print-quote" href="#">Print quote
											<i class="fas fa-print bright-yellow fa-2x"></i></a></div>

								</div>
							</div></div>
					</div>
				</div>
		</div>
		</form>		<br>

	</div>
	</div>
</article>

<!---PRINT MODAL -->
<div class="full reveal" id="print-quote" data-reveal>

	<div id="printThis">
		<div class="grid-container border padding-collapse">
			<div class="grid-x">
				<div class="large-6 cell custom-flex">
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-6 cell border">
							<img src="images/pcbprime-print-logo.png" class="print-logo" alt="PCB Prime Logo"/></div>
						<div class="large-6 cell border">
							<div class="grid-x grid-padding-x grid-padding-y">
								<div class="large-12 cell dark-blue">PCB Prime Headquarters</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y">
								<div class="large-12 cell">
									<br>
									<small>13900 E. Florida Ave | Suite F | Aurora, CO 80012</small>
									<small>
										Contact : Scott Walsh <br> Email : scott@pcbprime.com
										<br> Direct: 303-862-8925 Fax: 303-952-506
									</small>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-3 cell text-right border">
							Company :<br> Contact :<br> Part Number :
						</div>
						<div class="large-9 cell border">
							{{Controltek}}<br> {{Angela Striker}}<br> {{180-2908}}
							<div class="float-right">{{Rev A00}}</div>
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y flex">
						<div class="large-3 cell text-right border">
							Email :<br> Phone :<br> Description :
						</div>
						<div class="large-9 cell border">
							{{angela@controltek}}<br> {{555-555-5555}}<br> {{01300-00200-00018}}
						</div>
					</div>
				</div>
				<div class="large-6 cell custom-flex">
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border"> Quality Service You can Depend On</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-3 cell border">
							Incredible Pricing High Quality Fast Turn Times Even Faster Quotes Aluminum Flex / Rigid Flex
						</div>
						<div class="large-6 cell border custom-flex">
							<div class="grid-padding-x  grid-padding-y">
								<div class="large-12 cell light-blue border">Always Free Tooling and Free Electrical Test!</div>
								<div class="large-12 cell border flex">
									It's easy to move your legacy parts to us. We'll match your stencil so you can start saving money today!
									<br><br><br></div>
							</div>
						</div>
						<div class="large-3 cell border">
							RoHS/REACH Compliant Conflict Mineral Free UL & ISO 9001 Certified Free DFM File Review Prototypes to Full Production
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border">
							Place your order as late as 9pm EST / 6pm PST !
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y flex">
						<div class="large-3 cell border">
							Quote Number<br> Gerber File Name
						</div>
						<div class="large-9 cell border">
							<span class="red-text">{{SW12345-1254}}</span>
							<div class="float-right">Date {{5/15/2018 14:47}}</div>
							<br> <span class="red-text">{{ZINC5_01300-00200-00018_GBR_REV A00_2up.zip}}</span><br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="grid-container padding-collapse border">
			<div class="grid-x">
				<div class="large-6">
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border">Quoted PCB Specifications
						</div>
					</div>
					<div class="grid-x"> <!-- two columns -->
						<div class="large-6 cell">
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									Layers
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									Board Size
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									Array Size
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									Array
								</div>
								<div class="large-6 cell">
									{{Y/N}}
								</div>
								<div class="large-6 cell">
									# In Array
								</div>
								<div class="large-6 cell">
									{{xx}}
								</div>
								<div class="large-6 cell">
									X-Outs Allowed
								</div>
								<div class="large-6 cell">
									{{Yes/No}}
									<div style="display:block; height:2px; "><!--Spacer --></div>
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									Scoring
								</div>
								<div class="large-6 cell">
									{{Yes}}
								</div>
								<div class="large-6 cell">
									Jump Scoring
								</div>
								<div class="large-6 cell">
									{{No}}
								</div>
								<div class="large-6 cell">
									Tab Rout
								</div>
								<div class="large-6 cell">
									{{Yes}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Material
								</div>
								<div class="large-7 cell">
									{{FR-4 (170°C Tg}}
								</div>
								<div class="large-5 cell">
									Final Thickness
								</div>
								<div class="large-7 cell">
									{{0.062in / 1.6mm}}
								</div>
								<div class="large-5 cell">
									Plating
								</div>
								<div class="large-7 cell">
									{{Immersion Gold}}
								</div>
								<div class="large-5 cell">
									Outer Cu Weigh
								</div>
								<div class="large-7 cell">
									{{2}}
								</div>
								<div class="large-5 cell">
									Inner Cu Weigh
								</div>
								<div class="large-7 cell">
									{{2}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Sides Mask
								</div>
								<div class="large-7 cell">
									{{Both}}
								</div>
								<div class="large-5 cell">
									Mask Color
								</div>
								<div class="large-7 cell">
									{{Matte Blue}}
								</div>
								<div class="large-5 cell">
									Sides Silkscreen
								</div>
								<div class="large-7 cell">
									{{Both}}
								</div>
								<div class="large-5 cell">
									Silkscreen Color
								</div>
								<div class="large-7 cell">
									{{Silkscreen Color}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Gold Fingers
								</div>
								<div class="large-7 cell">
									{{Yes #}}
								</div>
								<div class="large-5 cell">
									Gold Thicknes
								</div>
								<div class="large-7 cell">
									{{30u}}
								</div>
								<div class="large-5 cell">
									Bevel Angle
								</div>
								<div class="large-7 cell">
									{{30}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-5 cell">
									Min Trace/Space
								</div>
								<div class="large-7 cell">
									{{7mil / 0.18mm}}
								</div>
								<div class="large-5 cell">
									Min Hole Size
								</div>
								<div class="large-7 cell">
									{{10mil / 0.25mm}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-8 cell">
									# Holes Under 12 mil
								</div>
								<div class="large-4 cell">
									{{4}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									Plated Slots
								</div>
								<div class="large-6 cell">
									{{Yes}}{{#}}
								</div>
								<div class="large-6 cell">
									Plated Edges
								</div>
								<div class="large-6 cell">
									{{Yes}}{{#}}
								</div>
							</div>
						</div>
						<div class="large-6 cell custom-flex">
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-6 cell">
									RoHS
								</div>
								<div class="large-6 cell">
									{{Yes/No}}
								</div>
								<div class="large-6 cell">
									ITAR
								</div>
								<div class="large-6 cell">
									{{Yes/No}}
								</div>
								<div class="large-6 cell">
									IPC Inspection Class
								</div>
								<div class="large-6 cell">
									{{#}}
								</div>
								<div class="large-6 cell">
								</div>
								<div class="large-6 cell">
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-12 cell text-center">
									UL/Date Code/94V-0 Mark
								</div>
								<div class="large-12 cell text-center">
									Per Fab Print
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-8 cell">
									Controlled Impedance
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Controlled Dielectric
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Carbon Ink
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-8 cell">
									Castellated Holes
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Blind/Buried Vias
								</div>
								<div class="large-4 cell">
									{{Yes}}
								</div>
								<div class="large-8 cell">
									Via in Pad
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Mask Tented Vias
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Epoxy Plugged Vias
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Conductive Filled Vias
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
								<div class="large-8 cell">
									Counter Sinks/Bores
								</div>
								<div class="large-4 cell">
									{{No}}
								</div>
							</div>
							<div class="grid-x grid-padding-x grid-padding-y border">
								<div class="large-12 cell dark-blue">
									Special Notes
								</div>
							</div>
							<div class="flex grid-x grid-padding-x grid-padding-y border light-blue">
								<div class="large-12 cell">
									Notes that can be saved about the quote should be entered here.
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="large-6 cell custom-flex">
					<div class="grid-x grid-padding-x grid-padding-y dark-blue">
						<div class="large-12 cell border">Lead times in Business Days (Mon-Fri)
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y">
						<div class="large-12 cell">

							<div class="grid-x grid-padding-x grid-padding-y ">
								<div class="large-12 cell border dark-blue">Unit Pricing (Cost per individual board) Qty is per board not per array
								</div>
								<div class="large-12 cell border">
									<table class="pricing-table">
										<thead>
										<tr>
											<th>Qty</th>
											<th>2 Day</th>
											<th>3 Day</th>
											<th>4 Day</th>
											<th>5 Day</th>
											<th>8 Day</th>
											<th>10 Day</th>
											<th>15 Day</th>
											<th>20 Day</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										</tbody>
									</table>
								</div>
								<div class="large-12 cell border dark-blue">
									Lot Pricing (Unit Qty x Unit Price = Lot Price) Qty is per board not per array
								</div>
								<div class="large-12 cell border">
									<table class="pricing-table">
										<thead>
										<tr>
											<th>Qty</th>
											<th>2 Day</th>
											<th>3 Day</th>
											<th>4 Day</th>
											<th>5 Day</th>
											<th>8 Day</th>
											<th>10 Day</th>
											<th>15 Day</th>
											<th>20 Day</th>
										</tr>
										</thead>
										<tbody>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										<tr>
											<td data-label="qty">
												XX
											</td>
											<td data-label="2day">xxx.xx</td>
											<td data-label="3day">xxx.xx</td>
											<td data-label="4day">xxx.xx</td>
											<td data-label="5day">xxx.xx</td>
											<td data-label="8day">xxx.xx</td>
											<td data-label="10day">xxx.xx</td>
											<td data-label="15day">xxx.xx</td>
											<td data-label="20day">xxx.xx</td>
										</tr>
										</tbody>
									</table>
									<div class="large-12 cell">
										<div class="float-left"> Tooling NRE:
											<span style="text-decoration:line-through" class="red-text">$300</span>
										</div>
										<div class="float-right">100% Electrical Test:
											<span style="text-decoration:line-through" class="red-text">$300</span>
										</div>
									</div>
								</div>
								<div class="large-12 cell attention border">
									<span class="red-text lead">Free Tooling and Electrical Test</span><br> Free DFM (Design For Manufacturing) Review on Every Order
								</div>
								<div class="large-12 cell light-blue border">
									<h5 class="red-text">Ordering is Easy</h5>
									To place an order, just call, email or fax me. I will be happy to place your order for you. My direct contact information is below.<br>
									<div class="red-text"> Email :
										<a href="mailto:" scott@pcbprime.com" ">scott@pcbprime.com</a>
										<br>Direct : <a href="tel:3038628925">303-862-8925</a> <br>Fax : 303-952-5063
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="grid-x grid-padding-x grid-padding-y border flex">
						<div class="large-12 cell">
							<ul>
								<li>Quote valid for 30 days.</li>
								<li>Daily order cut off time is 9pm EST / 6pm PST. Day zero begins once files are approved by engineering.</li>
								<li>Costs and lead times do not include shipping. Standard UPS Domestic Rates and delivery times apply. FOB Aurora, CO USA.</li>
								<li class="red-text">If certain specs were not specifically stated in your files, we assumed our industry standard specs. If any specs are inaccurate, let us know immediately. It may not only affect the cost but also how your boards are constructed !</li>
							</ul>
							<div class="float-right">0 v-Prime 18.05.15</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<button class="close-button" data-close aria-label="Close reveal" type="button">
		<span aria-hidden="true">Close &times;</span>
	</button>

	<button style="margin:50px auto; width:auto; float:none; display:block;" class="button large align-center text-center" id="btnPrint">Print</button>
</div>
<script>

    document.getElementById("btnPrint").onclick = function () {
        printElement(document.getElementById("printThis"));

        window.print();
    }

    function printElement(elem) {
        var domClone = elem.cloneNode(true);

        var $printSection = document.getElementById("printSection");

        if (!$printSection) {
            var $printSection = document.createElement("div");
            $printSection.id = "printSection";
            document.body.appendChild($printSection);
        }

        $printSection.innerHTML = "";

        $printSection.appendChild(domClone);
    }
</script>



<?php include_once('includes/footer.php'); ?>
