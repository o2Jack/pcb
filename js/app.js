$(document).foundation();
/*!
 * Marka - v0.3.1
 * http://fian.my.id/marka
 *
 * Copyright 2014 Alfiana E. Sibuea and other contributors
 * Released under the MIT license
 * https://github.com/fians/marka/blob/master/LICENSE
 */
!function (a) {
    "use strict";

    function b(a, b) {
        return Array.prototype.forEach.call(a, b)
    }

    function c(a) {
        return "object" == typeof HTMLElement ? a instanceof HTMLElement : a && "object" == typeof a && null !== a && 1 === a.nodeType && "string" == typeof a.nodeName
    }

    function d(a, b) {
        a.setAttribute("data-color", b);
        var c = a.getAttribute("data-icon"), d = a.children.length, e = [];
        if (g[c].hasOwnProperty("invert")) switch (g[c].invert) {
            case"last":
                e = [d - 1];
                break;
            case"last-two":
                e = [d - 2, d - 1];
                break;
            default:
                e = g[c].invert
        }
        for (var f = 0; d > f; f++) {
            var h = b;
            -1 !== e.indexOf(f) && (h = a.getAttribute("data-bg")), a.children[f].setAttribute("style", "background-color:" + h)
        }
    }

    function e(b) {
        var c = b.parentNode, d = "rgba(0, 0, 0, 0)";
        do if (d = a.getComputedStyle(c).backgroundColor, c = c.parentNode, "rgba(0, 0, 0, 0)" !== d) break; while ("tagName" in c);
        ("rgba(0, 0, 0, 0)" === d || "transparent" === d) && (d = "rgb(255, 255, 255)"), b.setAttribute("data-bg", d)
    }

    function f(a) {
        if (this.elements = [], "string" == typeof a && (this.elements = document.querySelectorAll(a)), c(a) && this.elements.push(a), a instanceof Array) for (var d = 0; d < a.length; d++) c(a[d]) && this.elements.push(a[d]);
        if (!this.elements.length) throw Error("No element is selected.");
        return b(this.elements, function (a) {
            e(a), -1 === a.className.indexOf("marka") && (a.className += " marka ")
        }), this
    }

    var g = {
        circle: {block: 2},
        "circle-o": {block: 3, invert: [1]},
        "circle-o-filled": {block: 3, invert: [1]},
        "circle-minus": {block: 3, invert: "last"},
        "circle-plus": {block: 3, invert: "last-two"},
        "circle-times": {block: 3, invert: "last-two"},
        "circle-o-minus": {block: 4, invert: [1]},
        "circle-o-plus": {block: 4, invert: [1]},
        "circle-o-times": {block: 4, invert: [1]},
        square: {block: 2},
        "square-o": {block: 3, invert: [1]},
        "square-o-filled": {block: 3, invert: [1]},
        "square-minus": {block: 3, invert: "last"},
        "square-plus": {block: 3, invert: "last-two"},
        "square-times": {block: 3, invert: "last-two"},
        "square-check": {block: 3, invert: "last-two"},
        "square-o-minus": {block: 4, invert: [1]},
        "square-o-plus": {block: 4, invert: [1]},
        "square-o-times": {block: 4, invert: [1]},
        "square-o-check": {block: 4, invert: [1]},
        triangle: {block: 3},
        asterisk: {block: 3},
        minus: {block: 1},
        plus: {block: 2},
        times: {block: 2},
        check: {block: 2},
        sort: {block: 6},
        "sort-half": {block: 3},
        "signal-three-one": {block: 3},
        "signal-three-two": {block: 3},
        "signal-three": {block: 3},
        "signal-five-one": {block: 5},
        "signal-five-two": {block: 5},
        "signal-five-three": {block: 5},
        "signal-five-four": {block: 5},
        "signal-five": {block: 5},
        pause: {block: 2},
        angle: {block: 2},
        "angle-double": {block: 4},
        arrow: {block: 3},
        bars: {block: 3},
        chevron: {block: 2}
    };
    f.prototype.set = function (a) {
        var c = this;
        return b(this.elements, function (b) {
            b.setAttribute("data-icon", a);
            var f = b.getAttribute("data-color");
            f || (f = "rgb(0, 0, 0)", b.setAttribute("data-color", f));
            var h = b.children.length;
            if (g[a].block > h) for (var i = 0; i < g[a].block - h; i++) {
                var j = document.createElement("i");
                b.appendChild(j)
            }
            e(b), d(b, f), setTimeout(function () {
                b.className = b.className.replace("  ", " ").replace(/marka-icon-[\w-]+/, ""), b.className += "marka-icon-" + a + " ", "sizeValue" in c && b.setAttribute("style", "width:" + c.sizeValue + "px;height:" + c.sizeValue + "px;"), -1 === b.className.indexOf("marka-set") && setTimeout(function () {
                    b.className += "marka-set "
                }, 200)
            }, 10)
        }), this
    }, f.prototype.color = function (a) {
        return b(this.elements, function (b) {
            e(b), d(b, a)
        }), this
    }, f.prototype.size = function (a) {
        return this.sizeValue = a, b(this.elements, function (b) {
            b.setAttribute("style", "width:" + a + "px;height:" + a + "px;")
        }), this
    }, f.prototype.rotate = function (a) {
        return b(this.elements, function (b) {
            b.className = b.className.replace("  ", " ").replace(/marka-rotate-[\w]+/, ""), b.className += "marka-rotate-" + a + " "
        }), this
    }, a.Marka = f
}(window);


$('.quotes').slick({
    dots: false,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 6000,
    speed: 1000,
    slidesToShow: 1,
    adaptiveHeight: true,
    responsive: [
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});


$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    dots: false,
    asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
            }
        },
        {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
    ]
});

$('.breadcrumb-counter-nav-item').click(function () {
    $('.breadcrumb-counter-nav-item').removeClass('current');
    $(this).addClass('current');
});

$(document).ready(function () {

    if ($('#icon').length) {

        var triggerOpen = $('#input');
        var triggerClose = $('#dropdown-menu').find('li');
        var marka = $('#icon');

        // set initial Marka icon
        var m = new Marka('#icon');
        m.set('triangle').size(10);
        m.rotate('down');


        // trigger dropdown
        triggerOpen.add(marka).on('click', function (e) {
            e.preventDefault();
            $('#dropdown-menu').add(triggerOpen).toggleClass('open');


            if ($('#icon').hasClass("marka-icon-times")) {
                m.set('triangle').size(10);
            } else {
                m.set('times').size(15);
            }
        });

        triggerClose.on('click', function () {
            // set new placeholder for demo
            var options = $(this).find('a').html();
            triggerOpen.text(options);

            $('#dropdown-menu').add(triggerOpen).toggleClass('open');
            m.set('triangle').size(10);
        });
    }

});



