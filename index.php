<?php include_once('includes/header-home.php'); ?>
<div class="grid-container">
	<div class="grid-x">
		<div class="large-offset-4 large-8 hero-cta">
			<div class="welcome h3">Welcome to PCB Prime</div>
			<p class="h3 text-right">
				<i class="fas fa-circle bright-yellow"></i><span class="home-hero-subheader"> High Quality Low Cost</span>
			</p>
			<h1 class="home-hero-title text-right">Printed <span>Circuit Boards</span></h1>
			<p class="clearfix"><a href="/quote.php" class="home-hero-button button medium">Click here for instant quote
					<i class="fas fa-arrow-circle-right fa-2x"></i></a></p>
			<p class="text-right call-text">If you prefer to send us your files for a quote, or if you have manufacturing questions, please call us toll free at
				<a href="tel:5555555555" class="hero-phone">1.800.555.5555</a> or
				<a href="/contact.php">contact us</a> we will be glad to help!</p>
		</div>
	</div>
</div>
</header>
<article class="home-page-content">
	<div class="grid-container">
		<div class="grid-x">
			<div class="large-8 medium-9 cell">
				<div class="home-tab-container">
					<ul class="tabs" data-responsive-accordion-tabs="tabs small-accordion medium-tabs large-tabs" id="quote-tabs">
						<li class="tabs-title is-active"><a href="#panel1c" aria-selected="true">Instant PCB Quote</a>
						</li>
						<li class="tabs-title"><a href="#panel2c">Assembly Quote</a></li>
						<li class="tabs-title"><a href="#panel3c">Stencil Quote</a></li>
					</ul>
					<div class="tabs-content" data-tabs-content="quote-tabs">
						<div class="tabs-panel is-active" id="panel1c">
							<div class="grid-container">
								<div class="grid-x grid-margin-x align-middle">
									<div class="large-8">
										<!-- Form Here -->
										<form>
											<div class="grid-container">
												<div class="grid-x grid-margin-x">
													<div class="large-auto">
														<label>Quantity (enter up to 5 values)<span data-tooltip class="top help" tabindex="2" title="Up to 5 different quantities can be entered. Tip: Press TAB after each qty to quickly jump to the next available cell."><i class="fas fa-question-circle"></i></span></label>
														<div class="grid-container">
															<div class="grid-x">
																<div class="medium-2 small-padding-rt cell">
																	<input type="text"/>
																</div>
																<div class="medium-2 small-padding-rt cell">
																	<input type="text"/>
																</div>
																<div class="medium-2 small-padding-rt cell">
																	<input type="text"/>
																</div>
																<div class="medium-2 small-padding-rt cell">
																	<input type="text"/>
																</div>
																<div class="medium-2 small-padding-rt cell">
																	<input type="text"/>
																</div>
															</div>
														</div>
													</div>
													<div class="large-auto">
														<div class="grid-container">
															<div class="grid-x">
																<label>Number of Copper Layers <span data-tooltip
																			class="top help"
																			tabindex="2"
																			title="The total number of copper layers in your design. Tip: Try to design using an even number of layers (2, 4, 6, 8, etc.). Because the materials used in the manufacturing process already have copper on both sides, the cost for a 3-layer board is the same as for a 4-layer board, for example. Additionally, there can be warping concerns with boards that have odd numbered layer construction due to unbalanced copper distribution."><i
																				class="fas fa-question-circle"></i></span>
																	<select name="layers" id="layers">
																		<option value="-1" class="instant">Select...
																		</option>
																		<option value="0" class="instant">0</option>
																		<option value="1" class="instant">1</option>
																		<option value="2" class="instant">2</option>
																		<option value="3" class="instant">3</option>
																		<option value="4" class="instant">4</option>
																		<option value="5" class="instant">5</option>
																		<option value="6" class="instant">6</option>
																		<option value="7" class="instant">7</option>
																		<option value="8" class="instant">8</option>
																		<option value="9" class="custom">9</option>
																		<option value="10" class="custom">10</option>
																		<option value="11" class="custom">11</option>
																		<option value="12" class="custom">12</option>
																		<option value="13" class="custom">13</option>
																		<option value="14" class="custom">14</option>
																		<option value="15" class="custom">15</option>
																		<option value="16" class="custom">16</option>
																		<option value="17" class="custom">17</option>
																		<option value="18" class="custom">18</option>
																		<option value="19" class="custom">19</option>
																		<option value="20" class="custom">20</option>
																		<option value="21" class="custom">21</option>
																		<option value="22" class="custom">22</option>
																		<option value="23" class="custom">23</option>
																		<option value="24" class="custom">24</option>
																		<option value="25" class="custom">25</option>
																		<option value="26" class="custom">26</option>
																		<option value="27" class="custom">27</option>
																		<option value="28" class="custom">28</option>
																		<option value="29" class="custom">29</option>
																		<option value="30" class="custom">30</option>
																		<option value="31" class="custom">31</option>
																		<option value="32" class="custom">32</option>
																		<option value="33" class="custom">33</option>
																		<option value="34" class="custom">34</option>
																		<option value="35" class="custom">35</option>
																		<option value="36" class="custom">36</option>
																	</select> </label>
															</div>
														</div>
													</div>
												</div>
												<div class="grid-x grid-margin-x">
													<div class="large-auto">
														<label>Board Size <span data-tooltip class="top help"
																	tabindex="3"
																	title="Enter the overall dimensions for your board. Measure the total length and width along the X and Y axes. If your board is a single design and is step-and-repeated in an array, enter the size of a single board. Select the ARRAY check box in the section below and enter the overall size of the desired array, including any tooling rails, in the ARRAY SIZE area. Additionally, when ordering an array, you must select at least one scoring/routing method: Scoring, Jump Scoring, or Tab-routing."><i
																		class="fas fa-question-circle"></i></span></label>
														<div class="grid-container">
															<div class="grid-x">
																<div class="medium-5 small-padding-rt cell">
																	<input type="text"/>
																</div>
																<div class="medium-1 cell">
																	X
																</div>
																<div class="medium-5 small-padding-rt cell">
																	<input type="text"/>
																</div>
															</div>
														</div>
													</div>
													<div class="large-auto">
														<div class="grid-container">
															<div class="grid-x padded-amount">
																<label>Measurement Units <select>
																		<option value="inches">Inches</option>
																		<option value="mm">mm</option>
																	</select> </label>
															</div>
														</div>
													</div>
												</div>
										</form>
										<!--                                        End form-->
									</div>
								</div>
								<div class="large-4">
									<!-- Button Here                                       -->
									<button class="big-quote-button">Get instant quote
										<i class="fas fa-arrow-circle-right"></i></button>
								</div>
							</div>
						</div>
					</div>
					<div class="tabs-panel" id="panel2c">
						<div class="grid-container">
							<div class="grid-x grid-margin-x align-middle">
								<div class="large-8">
									Assembly Quote Place Holder
									<!-- Form Here                                       -->
									<form>
										<div class="grid-container">
											<div class="grid-x grid-margin-x">
												<div class="large-auto">
													<label>Quantity (enter up to 5 values)<span data-tooltip
																class="top help"
																tabindex="2"
																title="Up to 5 different quantities can be entered. Tip: Press TAB after each qty to quickly jump to the next available cell."><i class="fas fa-question-circle"></i></span></label>
													<div class="grid-container">
														<div class="grid-x">
															<div class="medium-2 small-padding-rt cell">
																<input type="text"/>
															</div>
															<div class="medium-2 small-padding-rt cell">
																<input type="text"/>
															</div>
															<div class="medium-2 small-padding-rt cell">
																<input type="text"/>
															</div>
															<div class="medium-2 small-padding-rt cell">
																<input type="text"/>
															</div>
															<div class="medium-2 small-padding-rt cell">
																<input type="text"/>
															</div>
														</div>
													</div>
												</div>
												<div class="large-auto">
													<div class="grid-container">
														<div class="grid-x">
															<label>Number of Copper Layers
																<span data-tooltip class="top help" tabindex="2" title="The total number of copper layers in your design. Tip: Try to design using an even number of layers (2, 4, 6, 8, etc.). Because the materials used in the manufacturing process already have copper on both sides, the cost for a 3-layer board is the same as for a 4-layer board, for example. Additionally, there can be warping concerns with boards that have odd numbered layer construction due to unbalanced copper distribution."><i class="fas fa-question-circle"></i></span>
																<select name="layers" id="layers">
																	<option value="-1" class="instant">Select...
																	</option>
																	<option value="0" class="instant">0</option>
																	<option value="1" class="instant">1</option>
																	<option value="2" class="instant">2</option>
																	<option value="3" class="instant">3</option>
																	<option value="4" class="instant">4</option>
																	<option value="5" class="instant">5</option>
																	<option value="6" class="instant">6</option>
																	<option value="7" class="instant">7</option>
																	<option value="8" class="instant">8</option>
																	<option value="9" class="custom">9</option>
																	<option value="10" class="custom">10</option>
																	<option value="11" class="custom">11</option>
																	<option value="12" class="custom">12</option>
																	<option value="13" class="custom">13</option>
																	<option value="14" class="custom">14</option>
																	<option value="15" class="custom">15</option>
																	<option value="16" class="custom">16</option>
																	<option value="17" class="custom">17</option>
																	<option value="18" class="custom">18</option>
																	<option value="19" class="custom">19</option>
																	<option value="20" class="custom">20</option>
																	<option value="21" class="custom">21</option>
																	<option value="22" class="custom">22</option>
																	<option value="23" class="custom">23</option>
																	<option value="24" class="custom">24</option>
																	<option value="25" class="custom">25</option>
																	<option value="26" class="custom">26</option>
																	<option value="27" class="custom">27</option>
																	<option value="28" class="custom">28</option>
																	<option value="29" class="custom">29</option>
																	<option value="30" class="custom">30</option>
																	<option value="31" class="custom">31</option>
																	<option value="32" class="custom">32</option>
																	<option value="33" class="custom">33</option>
																	<option value="34" class="custom">34</option>
																	<option value="35" class="custom">35</option>
																	<option value="36" class="custom">36</option>
																</select> </label>
														</div>
													</div>
												</div>
											</div>
											<div class="grid-x grid-margin-x">
												<div class="large-auto">
													<label>Board Size
														<span data-tooltip class="top help" tabindex="2" title="Enter the overall dimensions for your board. Measure the total length and width along the X and Y axes. If your board is a single design and is step-and-repeated in an array, enter the size of a single board. Select the ARRAY check box in the section below and enter the overall size of the desired array, including any tooling rails, in the ARRAY SIZE area. Additionally, when ordering an array, you must select at least one scoring/routing method: Scoring, Jump Scoring, or Tab-routing."><i class="fas fa-question-circle"></i></span></label>
													<div class="grid-container">
														<div class="grid-x">
															<div class="medium-5 small-padding-rt cell">
																<input type="text"/>
															</div>
															<div class="medium-1 cell">
																X
															</div>
															<div class="medium-5 small-padding-rt cell">
																<input type="text"/>
															</div>
														</div>
													</div>


												</div>
												<div class="large-auto">
													<div class="grid-container">
														<div class="grid-x padded-amount">
															<label>Measurement Units <select>
																	<option value="inches">Inches</option>
																	<option value="mm">mm</option>
																</select> </label>
														</div>
													</div>
												</div>
											</div>


									</form>
									<!-- End form-->
								</div>
							</div>
							<div class="large-4">
								<!-- Button Here -->
								<button class="big-quote-button">Get instant quote
									<i class="fas fa-arrow-circle-right"></i></button>
							</div>
						</div>
					</div>
				</div>
				<div class="tabs-panel" id="panel3c">
					<div class="grid-container">
						<div class="grid-x grid-margin-x align-middle">
							<div class="large-8">
								Stencil Quote Place Holder
								<!-- Form Here -->
								<form>
									<div class="grid-container">
										<div class="grid-x grid-margin-x">
											<div class="large-auto">
												<label>Quantity (enter up to 5 values)<span data-tooltip
															class="top help"
															tabindex="2"
															title="Up to 5 different quantities can be entered. Tip: Press TAB after each qty to quickly jump to the next available cell."><i
																class="fas fa-question-circle"></i></span></label>
												<div class="grid-container">
													<div class="grid-x">
														<div class="medium-2 small-padding-rt cell">
															<input type="text"/>
														</div>
														<div class="medium-2 small-padding-rt cell">
															<input type="text"/>
														</div>
														<div class="medium-2 small-padding-rt cell">
															<input type="text"/>
														</div>
														<div class="medium-2 small-padding-rt cell">
															<input type="text"/>
														</div>
														<div class="medium-2 small-padding-rt cell">
															<input type="text"/>
														</div>
													</div>
												</div>
											</div>
											<div class="large-auto">
												<div class="grid-container">
													<div class="grid-x">
														<label>Number of Copper Layers <span data-tooltip
																	class="top help"
																	tabindex="2"
																	title="The total number of copper layers in your design. Tip: Try to design using an even number of layers (2, 4, 6, 8, etc.). Because the materials used in the manufacturing process already have copper on both sides, the cost for a 3-layer board is the same as for a 4-layer board, for example. Additionally, there can be warping concerns with boards that have odd numbered layer construction due to unbalanced copper distribution."><i
																		class="fas fa-question-circle"></i></span>
															<select name="layers" id="layers">
																<option value="-1" class="instant">Select...</option>
																<option value="0" class="instant">0</option>
																<option value="1" class="instant">1</option>
																<option value="2" class="instant">2</option>
																<option value="3" class="instant">3</option>
																<option value="4" class="instant">4</option>
																<option value="5" class="instant">5</option>
																<option value="6" class="instant">6</option>
																<option value="7" class="instant">7</option>
																<option value="8" class="instant">8</option>
																<option value="9" class="custom">9</option>
																<option value="10" class="custom">10</option>
																<option value="11" class="custom">11</option>
																<option value="12" class="custom">12</option>
																<option value="13" class="custom">13</option>
																<option value="14" class="custom">14</option>
																<option value="15" class="custom">15</option>
																<option value="16" class="custom">16</option>
																<option value="17" class="custom">17</option>
																<option value="18" class="custom">18</option>
																<option value="19" class="custom">19</option>
																<option value="20" class="custom">20</option>
																<option value="21" class="custom">21</option>
																<option value="22" class="custom">22</option>
																<option value="23" class="custom">23</option>
																<option value="24" class="custom">24</option>
																<option value="25" class="custom">25</option>
																<option value="26" class="custom">26</option>
																<option value="27" class="custom">27</option>
																<option value="28" class="custom">28</option>
																<option value="29" class="custom">29</option>
																<option value="30" class="custom">30</option>
																<option value="31" class="custom">31</option>
																<option value="32" class="custom">32</option>
																<option value="33" class="custom">33</option>
																<option value="34" class="custom">34</option>
																<option value="35" class="custom">35</option>
																<option value="36" class="custom">36</option>
															</select> </label>
													</div>
												</div>
											</div>
										</div>
										<div class="grid-x grid-margin-x">
											<div class="large-auto">
												<label>Board Size <span data-tooltip class="top help" tabindex="2"
															title="Enter the overall dimensions for your board. Measure the total length and width along the X and Y axes. If your board is a single design and is step-and-repeated in an array, enter the size of a single board. Select the ARRAY check box in the section below and enter the overall size of the desired array, including any tooling rails, in the ARRAY SIZE area. Additionally, when ordering an array, you must select at least one scoring/routing method: Scoring, Jump Scoring, or Tab-routing."><i
																class="fas fa-question-circle"></i></span></label>
												<div class="grid-container">
													<div class="grid-x">
														<div class="medium-5 small-padding-rt cell">
															<input type="text"/>
														</div>
														<div class="medium-1 cell">
															X
														</div>
														<div class="medium-5 small-padding-rt cell">
															<input type="text"/>
														</div>
													</div>
												</div>
											</div>
											<div class="large-auto">
												<div class="grid-container">
													<div class="grid-x padded-amount">
														<label>Measurement Units <select>
																<option value="inches">Inches</option>
																<option value="mm">mm</option>
															</select> </label>
													</div>
												</div>
											</div>
										</div>
								</form>
								<!--End form-->
							</div>
						</div>
						<div class="large-4">
							<!-- Button Here -->
							<button class="big-quote-button">Get instant quote <i class="fas fa-arrow-circle-right"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="large-3 medium-3  large-offset-1 cell">
		<a href="#" class="homepage-ad"><img src="images/homepage-special.png" alt="Prototype Special"/></a>
	</div>
</article>
<div class="grid-container" id="services">
	<div class="grid-x grid-padding-x" data-equalizer data-equalize-on="medium">
		<div class="large-4 medium-4 cell bullet-list">
			<img src="images/red-board.jpg" alt="PCB Capabilities" class="service-image align-center text-center"/>
			<h3><i class="fas fa-circle header-circle"></i> Rigid PCB capabilities</h3>
			<ul class="fa-ul" data-equalizer-watch >
				<li><i class="far fa-circle"></i> Up to 42 layers</li>
				<li><i class="far fa-circle"></i> RoHS Finishes</li>
				<li><i class="far fa-circle"></i> (ENIG, Immersion Silver, OSP.</li>
				<li><i class="far fa-circle"></i> Lead Free Solder, Immersion Tin)</li>
				<li><i class="far fa-circle"></i> Controlled Impedance</li>
				<li><i class="far fa-circle"></i> Blind & Buried Vias</li>
				<li><i class="far fa-circle"></i> Class III & Class III</li>
				<li><i class="far fa-circle"></i> ISO 9001-2000</li>
			</ul>
			<a href="#" class="dark-more-button">Learn more
				<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a>
		</div>
		<div class="large-4 medium-4 cell bullet-list">
			<img src="images/assembly-board.jpg" alt="Assembly Capabilities" class="service-image align-center text-center"/>
			<h3><i class="fas fa-circle header-circle"></i> PCB Assembly Services</h3>
			<ul class="fa-ul" data-equalizer-watch >
				<li><i class="far fa-circle"></i> Up to 42 layers</li>
				<li><i class="far fa-circle"></i> RoHS Finishes</li>
				<li><i class="far fa-circle"></i> (ENIG, Immersion Silver, OSP.</li>
				<li><i class="far fa-circle"></i> Lead Free Solder, Immersion Tin)</li>

			</ul>
			<a href="#" class="dark-more-button">Learn more
				<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a>
		</div>
		<div class="large-4 medium-4 cell bullet-list">
			<img src="images/other-board.jpg" alt="Other Services" class="service-image align-center text-center"/>
			<h3><i class="fas fa-circle header-circle"></i> Other Services</h3>
			<ul class="fa-ul" data-equalizer-watch>
				<li><i class="far fa-circle"></i> Up to 42 layers</li>
				<li><i class="far fa-circle"></i> RoHS Finishes</li>
				<li><i class="far fa-circle"></i> (ENIG, Immersion Silver, OSP.</li>
				<li><i class="far fa-circle"></i> Lead Free Solder, Immersion Tin)</li>
				<li><i class="far fa-circle"></i> Controlled Impedance</li>
				<li><i class="far fa-circle"></i> Blind & Buried Vias</li>
				<li><i class="far fa-circle"></i> Class III & Class III</li>
				<li><i class="far fa-circle"></i> ISO 9001-2000</li>
			</ul>
			<a href="#" class="dark-more-button">Learn more
				<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a>
		</div>
	</div>
</div>
<div id="solutions">
	<div class="solutions-wrapper text-center">
		<h2>Our <span class="bright-yellow">Solutions</span></h2>
		<p class="bright-yellow">We are committed to the continuous improvement of our services to our customers</p>
		<img src="images/white-divider.png" alt="-----divider----" class="divider align-center"/>
		<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy
			<br> nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
		</p>
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="large-3 medium-6 small-6 cell">
					<img src="images/easy-quote-icon.svg" alt="Easy Quote Icon"/>
					<p><strong>Easy Quote Form</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
				<div class="large-3 medium-6 small-6 cell">
					<img src="images/instant-price-icon.svg" alt="Instant Price Icon"/>
					<p><strong>Instant Pricing</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
				<div class="large-3 medium-6 small-6 cell">
					<img src="images/testing-icon.svg" alt="Testing Icon"/>
					<p><strong>No Tooling or Test Charges</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
				<div class="large-3 medium-6 small-6 end cell">
					<img src="images/no-hidden-fees-icon.svg" alt="No Hidden Fees Icon"/>
					<p><strong>No Hidden Fees</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
			</div>
			<div class="grid-x grid-padding-x">
				<div class="large-3 medium-6 small-6 cell">
					<img src="images/proto-production-icon.svg" alt="Proto and Production Icon"/>
					<p><strong>Proto and Production</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
				<div class="large-3 medium-6 small-6 cell">
					<img src="images/fast-lead-icon.svg" alt="Fast Lead Times"/>
					<p><strong>Fast Lead Times</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
				<div class="large-3 medium-6 small-6 cell">
					<img src="images/world-class-icon.svg" alt="World Class Quality Icon"/>
					<p><strong>World Class Quality</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
				<div class="large-3 medium-6 small-6 cell">
					<img src="images/certified-icon.svg" alt="ISO and UL Certified PCBs Icon"/>
					<p><strong>ISO and UL Certified PCBs</strong></p>
					<p class="small">Lorem ipm dolor consectetur adipiing lit Sunt sed ad possimus ils magm mai dolor ilmesy magnam maores.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="pricing">
	<h2 class="text-center">Prime <span>Pricing</span></h2>
	<p class="text-center bright-red">No hidden fees</p>
	<img src="images/dark-divider.png" alt="-----divider----" class="align-center divider"/>
	<p class="text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy<br> nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
	</p>
	<br>
	<div class="grid-container">
		<div class="grid-x grid-margin-x">
			<div class="large-3 medium-6 cell">
				<ul class="pricing-table">
					<li class="title">2 Layer</li>
					<li class="price">
						<span class="bright-yellow">$ </span>29<br><span class="float-left">Min Qty 4</span></li>
					<li class="description"></li>
					<li>8 Day Lead Time</li>
					<li>$116 Total</li>
					<li><a class="dark-more-button" href="#">Buy Now
							<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>
				</ul>
			</div>
			<div class="large-3 medium-6 cell">
				<ul class="pricing-table">
					<li class="title">4 Layer</li>
					<li class="price">
						<span class="bright-yellow">$ </span>54<br><span class="float-left">Min Qty 4</span></li>
					<li class="description"></li>
					<li>10 Day Lead Time</li>
					<li>$216 Total</li>
					<li><a class="dark-more-button" href="#">Buy Now
							<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>
				</ul>
			</div>
			<div class="large-3 medium-6 cell">
				<ul class="pricing-table">
					<li class="title">6 layer</li>
					<li class="price">
						<span class="bright-yellow">$ </span>99<br><span class="float-left">Min Qty 4</span></li>
					<li class="description"></li>
					<li>11 Day Lead Time</li>
					<li>$396 Total</li>
					<li><a class="dark-more-button" href="#">Buy Now
							<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>
				</ul>
			</div>
			<div class="large-3 medium-6 cell">
				<ul class="pricing-table">
					<li class="title">8 Layer</li>
					<li class="price">
						<span class="bright-yellow">$ </span>129<br><span class="float-left">Min Qty 4</span></li>
					<li class="description"></li>
					<li>12 Day Lead Time</li>
					<li>$512 Total</li>
					<li><a class="dark-more-button" href="#">Buy Now
							<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div id="testimonials">
	<h2 class="text-center">Customer <span class="bright-yellow">testimonials</span></h2>
	<img src="images/testimonial-divider.png" alt="-----divider----" class="align-center divider"/>
	<section class="quotes">
		<div class="testimony">
			<blockquote>PCB Prime provided a custom PCB promptly and professionally and at a much lower price than others.
			</blockquote>
			<div></div>
			<cite>Guy Mann<br>Product Manager - <a href="#">www.edudesite.com</a></cite></div>
		<div class="testimony">
			<blockquote>PCB Prime provided a custom PCB promptly and professionally and at a much lower price than others.
			</blockquote>
			<div></div>
			<cite>Guy Mann<br>Product Manager - <a href="#">www.edudesite.com</a></cite></div>
		<div class="testimony">
			<blockquote>PCB Prime provided a custom PCB promptly and professionally and at a much lower price than others.
			</blockquote>
			<div></div>
			<cite>Guy Mann<br>Product Manager - <a href="#">www.edudesite.com</a></cite></div>
		<div class="testimony">
			<blockquote>PCB Prime provided a custom PCB promptly and professionally and at a much lower price than others.
			</blockquote>
			<div></div>
			<cite>Guy Mann<br>Product Manager - <a href="#">www.edudesite.com</a></cite></div>
	</section>
</div>
<div id="techtips">
	<h2 class="text-center">Tech <span>Tips</span></h2>
	<p class="bright-red text-center">Helpful tips for your next project</p>
	<img src="images/dark-divider.png" alt="-----divider----" class="align-center divider"/>
	<p class="text-center">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy<br> nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
	</p>
	<div class="grid-container hide-for-small-only">
		<div>
			<section class="slider tips slider-for">
				<div class="grid-container">
					<div class="grid-x">
						<div class="large-6"><strong class="all-caps">Array Design Tips</strong>
							<p>The primary reason for having your boards delivered in an array is to make automated assembly faster and less expensive. Running an array of boards through a pick-and-place machine is far more efficient than sending them through one at a time. Arrays are ...</p>
							<a href="#" class="dark-more-button float-right">Learn More
								<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>						</ul></a>
						</div>
						<div class="large-5 large-offset-1">
							<img src="images/techtip-image-1.jpg" class="techtip-image show-for-large" alt="Tech Tip Image One"/>
						</div>
					</div>
				</div>

				<div class="grid-container">
					<div class="grid-x">
						<div class="large-6"><strong class="all-caps">Controlled Dielectric or Controlled Impedance?</strong>
							<p>What is the difference between Controlled Dielectric and Controlled Impedance regarding PCB fabrication? The purpose of controlling dielectric or impedance is essentially the same, to achieve a target impedance on one or more signal lines on a printed... read more ></p>
							<a href="#" class="dark-more-button float-right">Learn More
								<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>						</ul></a>
						</div>
						<div class="large-5 large-offset-1">
							<img src="images/techtip-image-1.jpg" class="techtip-image show-for-large" alt="Tech Tip Image One"/>
						</div>
					</div>
				</div>

				<div class="grid-container">
					<div class="grid-x">
						<div class="large-6"><strong class="all-caps">Via Tenting, Plugging, and Filling</strong>
							<p>There are many reasons a printed circuit board designer might want to have a via tented, plugged or filled. First, let's start with defining these terms since they can be frequently misused and misunderstood.... read more ></p>
							<a href="#" class="dark-more-button float-right">Learn More
								<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>						</ul></a>
						</div>
						<div class="large-5 large-offset-1">
							<img src="images/techtip-image-1.jpg" class="techtip-image show-for-large" alt="Tech Tip Image One"/>
						</div>
					</div>
				</div>

				<div class="grid-container">
					<div class="grid-x">
						<div class="large-6"><strong class="all-caps">Countersink vs. Counterbore</strong>
								<p>A countersink is a cone shaped hole cut into the laminate. It is typically used to allow the tapered head of a screw to sit flush with the top of the laminate. By comparison, a counterbore makes a flat-bottomed hole and its sides are drilled straight... read more ></p>
							<a href="#" class="dark-more-button float-right">Learn More
								<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>						</ul></a>
						</div>
						<div class="large-5 large-offset-1">
							<img src="images/techtip-image-1.jpg" class="techtip-image show-for-large" alt="Tech Tip Image One"/>
						</div>
					</div>
				</div>

				<div class="grid-container">
					<div class="grid-x">
						<div class="large-6"><strong class="all-caps">Solder Mask</strong>
							<p>Solder Mask is a protective coating applied to the bare printed circuit board. Bare boards are covered with mask to prevent accidental solder bridging during assembly and to help protect the board from the environment. ... read more ></p>
								<a href="#" class="dark-more-button float-right">Learn More
									<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a></li>						</ul></a>
						</div>
						<div class="large-5 large-offset-1">
							<img src="images/techtip-image-1.jpg" class="techtip-image show-for-large" alt="Tech Tip Image One"/>
						</div>
					</div>
				</div>







			</section>
		</div>
		<div class="slider slider-nav">
			<div class="thumb-container thumb<?php echo rand(1,3);?>">
				<div class="vertical-align-wrap">
					<div class="vertical-align align-middle">
						Array Design Tips
					</div>
				</div>
			</div>
			<div class="thumb-container thumb<?php echo rand(1,3);?>">
				<div class="vertical-align-wrap">
					<div class="vertical-align align-middle">
						Controlled Dielectric or Controlled Impedance?
					</div>
				</div>
			</div>
			<div class="thumb-container thumb<?php echo rand(1,3);?>">
				<div class="vertical-align-wrap">
					<div class="vertical-align align-middle">
						Via Tenting, Plugging, and Filling
					</div>
				</div>
			</div>
			<div class="thumb-container thumb<?php echo rand(1,3);?>">
				<div class="vertical-align-wrap">
					<div class="vertical-align align-middle">
						Countersink vs. Counterbore
					</div>
				</div>
			</div>
			<div class="thumb-container thumb<?php echo rand(1,3);?>">
				<div class="vertical-align-wrap">
					<div class="vertical-align align-middle">
						Solder Mask
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="grid-container">
		<div class="grid-x">
		<a href="#" class="dark-more-button" style="margin-left:auto; margin-right:auto;">See All Tips
			<i class="fas fa-arrow-circle-right bright-yellow fa-2x"></i></a>
		</div>

	</div>
</div>
<?php include_once('includes/footer.php'); ?>
